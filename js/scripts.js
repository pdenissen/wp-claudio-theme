var claudio = claudio || {},
	claudioShortCode = claudioShortCode || {};
( function ( $ ) {
	'use strict';

	$( function() {
		var $body = $( 'body' );

		/**
		 * Off canvas nav toggle
		 */
		$( '#toggle-nav' ).on( 'click', function( e ) {
			e.preventDefault();

			$body.toggleClass( 'display-nav' );
			$( this ).toggleClass( 'active' );
		} );

		/**
		 * Off canvas cart toggle
		 */
		$( '#toggle-cart' ).on( 'click', function( e ) {
			e.preventDefault();

			$body.toggleClass( 'display-cart' );
			$( this ).toggleClass( 'active' );
		} );

		// Fit Video
		$( '.entry-header .format-video' ).fitVids( { customSelector: 'iframe'} );

		/**
		 * Hide cart or nav when click to off canvas layer
		 */
		$( '#off-canvas-layer' ).on( 'click', function( e ) {
			e.preventDefault();

			$body.removeClass( 'display-nav display-cart' );
			$( '#toggle-nav, #toggle-cart' ).removeClass( 'active' );
		} );

		/**
		 * Search toggle
		 */
		$( '.menu-item-search' ).on( 'click', '.fa-search', function( e ) {
			e.preventDefault();

			$( this ).closest( '.menu-item-search' ).toggleClass( 'show-search-form' );
		} );

		/**
		 * Toggle video banner play button
		 */
		$( '.video-banner' ).on( 'click' , 'i', function( e ) {
			e.preventDefault();
			var $el = $( this ),
				$banner = $el.closest( '.video-banner' ),
				video = $el.siblings( 'video' )[0];

			window.console.log(video.paused);

			if ( video.paused ) {
				$el.addClass( 'fa-pause' );
				$banner.addClass( 'playing' );
				video.play();
			} else {
				$el.removeClass( 'fa-pause' );
				$banner.removeClass( 'playing' );
				video.pause();
			}
		} );

		/* ToolTip */
		$( '.yith-wcwl-add-to-wishlist .add_to_wishlist' ).attr( 'data-original-title', $( '.yith-wcwl-add-to-wishlist .add_to_wishlist' ).html() ).attr( 'rel', 'tooltip' );
		$( '.yith-wcwl-wishlistaddedbrowse a' ).attr( 'data-original-title', $( '.yith-wcwl-wishlistaddedbrowse a' ).html() ).attr( 'rel', 'tooltip' );
		$( '.yith-wcwl-wishlistexistsbrowse a' ).attr( 'data-original-title', $( '.yith-wcwl-wishlistexistsbrowse a' ).html() ).attr( 'rel', 'tooltip' );
		$( '.woocommerce .compare.button' ).attr( 'data-original-title', $( '.woocommerce .compare.button' ).html() ).attr( 'rel', 'tooltip' );

		$( '[rel=tooltip]' ).tooltip({ offsetTop: -5 });

		/**
		 * Shop view toggle
		 */
		$( '.shop-view' ).on( 'click', 'a', function( e ) {
			e.preventDefault();
			var $el = $( this ),
				view = $el.data( 'view' );

			if ( $el.hasClass( 'current' ) ) {
				return;
			}

			$el.addClass( 'current' ).siblings().removeClass( 'current' );
			$body.removeClass( 'shop-view-grid shop-view-list' ).addClass( 'shop-view-' + view );

			document.cookie = 'shop_view=' + view + ';domain=' + window.location.host + ';path=/';
		} );

		singleThumbCarousel();

		productsCarousel();

		productsAjax();

		postsCarousel();

		imagesCarousel();

		// Flex slider for gallery
		$( '.format-gallery-slider .slides' ).owlCarousel( {
			singleItem: true,
			slideSpeed : 800,
			navigation: true,
			pagination: false,
			autoPlay: true,
			paginationSpeed : 1000,
			navigationText: ['<span class="fa fa-arrow-left"></span>', '<span class="fa fa-arrow-right"></span>']
		} );

		/**
		 * Product quick view popup
		 */
		var $modal = $( '#modal' ),
			$modalBody = $modal.find( '.modal-body' );

		// Open product single modal
		$( '.product' ).on( 'click', '.product-quick-view', function( e ) {
			e.preventDefault();

			$modal.fadeIn().addClass( 'in' );
			$modalBody.html( '<div class="ajax-loading"><i class="fa fa-spin fa-spinner"></i></div>' );
			$body.addClass( 'modal-open' );
			$.get( $( this ).attr( 'data-href' ), function( response ) {
				if ( ! response ) {
					return;
				}

				var $content = $( response ).find( '.product-details' );

				$modalBody.html( $content );

				singleThumbCarousel();

				$( 'a[data-rel^="prettyPhoto"]' ).prettyPhoto({
					hook: 'data-rel',
					social_tools: false,
					theme: 'pp_woocommerce',
					horizontal_padding: 20,
					opacity: 0.8,
					deeplinking: false
				});

				if ( typeof wc_add_to_cart_variation_params !== 'undefined' ) {
					$( '.variations_form' ).wc_variation_form();
					$( '.variations_form .variations select' ).change();
				}

				var pubid = $modal.attr( 'data-id' );
				if( pubid !== '' ) {
					var script = '//s7.addthis.com/js/300/addthis_widget.js#pubid=' + pubid;
					if (window.addthis) {
					    window.addthis = null;
					    window._adr = null;
					    window._atc = null;
					    window._atd = null;
					    window._ate = null;
					    window._atr = null;
					    window._atw = null;
					}
					$.getScript(script);
				}
			} );
		} );

		// Close portfolio modal
		$modal.on( 'click', 'button.close', function( e ) {
			e.preventDefault();

			$modal.fadeOut( 500, function() {
				$body.removeClass( 'modal-open' );
				$modal.removeClass( 'in' );

				// Trigger resize event on $window to make isotope mansory works correctly
				$( window ).trigger( 'resize' );
			} );
		} );

		/**
		 * Init product carousel
		 */
		function productsCarousel() {
			if ( claudioShortCode.length === 0 || typeof claudioShortCode.productsCarousel === 'undefined' ) {
				return;
			}
			$.each( claudioShortCode.productsCarousel, function ( id, productsCarousel ) {

				var autoplay = productsCarousel.autoplay ? 4000 : false,
					hideNavigation = productsCarousel.navigation,
					itemsSmall = 1;

				if( productsCarousel.number > 1 ) {
					itemsSmall = productsCarousel.number - 1;
				}

				$( document.getElementById( id ) ).find( '.products').owlCarousel({
					direction: claudio.direction,
					items: productsCarousel.number,
					slideSpeed : 800 ,
					navigation: hideNavigation,
					pagination: false,
					autoPlay: autoplay,
        			paginationSpeed : 1000,
					navigationText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
					itemsDesktopSmall: [979, itemsSmall],
					itemsDesktop: [1199, productsCarousel.number]
				});

			} );
		}


		/**
		 * Init product ajax
		 */
		function productsAjax() {
			var paged = 1;
			$( '.claudio-products-ajax').on( 'click', '.view-more-products', function( e ) {
				e.preventDefault();

				paged++;
				var id = $( this ).parents( '.claudio-products-ajax' ).attr( 'id' ),
					number = $( this ).attr( 'data-number' ),
					href = $( this ).attr( 'href' ) + '&page=' + paged;
				$( this ).parents( '.claudio-products-ajax' ).find( '.ajax-loading' ).show();
				$.get(
					href,
					function( response ) {
						var $content = $( response ).find( '#' + id ).find( '.products' ).html();
						if( $( response ).find( '#' + id ).find( '.products .product' ).length < number ){
							$( document.getElementById( id ) ).find( '.view-more-products' ).hide();
						}

						$( document.getElementById( id ) ).find( '.products' ).append( $content );
						$( document.getElementById( id ) ).find( '.ajax-loading' ).hide();
					}
				);
			} );
		}

		/**
		 * Init single Thumb carousel
		 */
		function singleThumbCarousel() {
			$( 'div.product .thumbnails' ).owlCarousel( {
				direction: claudio.direction,
				items: 4,
				itemsCustom: [[0, 2], [568, 3], [992, 4]],
				slideSpeed : 800 ,
				navigation: true,
				pagination: false,
				paginationSpeed : 1000,
				navigationText: ['<span class="fa fa-chevron-left"></span>', '<span class="fa fa-chevron-right"></span>']
			} );
		}

		/**
		 * Init posts carousel
		 */
		function postsCarousel() {
			if ( claudioShortCode.length === 0 || typeof claudioShortCode.postsCarousel === 'undefined' ) {
				return;
			}
			$.each( claudioShortCode.postsCarousel, function ( id, postsCarousel ) {
				var autoplay = postsCarousel.autoplay ? 4000 : false,
					hideNavigation = postsCarousel.navigation;

				$( document.getElementById( id ) ).find( '.bb-owl-list').owlCarousel({
					items: postsCarousel.number,
					slideSpeed : 800 ,
					navigation: hideNavigation,
					pagination: true,
					autoPlay: autoplay,
        			paginationSpeed : 1000,
					navigationText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
					itemsDesktopSmall: [979, 2],
					itemsDesktop: [1199, postsCarousel.number]
				});

			} );
		}

		/**
		 * Init Images Carousel
		 */
		function imagesCarousel() {
			if ( claudioShortCode.length === 0 || typeof claudioShortCode.imagesCarousel === 'undefined' ) {
				return;
			}
			$.each( claudioShortCode.imagesCarousel, function ( id, imagesCarousel ) {
				var autoplay = imagesCarousel.autoplay ? 4000 : false,
					hideNavigation = imagesCarousel.navigation;

				$( document.getElementById( id ) ).find( '.bb-owl-list').owlCarousel({
					items: imagesCarousel.number,
					slideSpeed : 800 ,
					navigation: hideNavigation,
					pagination: true,
					autoPlay: autoplay,
        			paginationSpeed : 1000,
					navigationText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>']
				});
			} );
		}
	} );
} )( jQuery );
