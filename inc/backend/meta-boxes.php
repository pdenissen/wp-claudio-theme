<?php
/**
 * Handle theme meta boxes
 * Add new meta boxes for posts, pages, products
 * Load scripts for meta boxes
 *
 * @package Claudio
 */

/**
 * Register meta boxes
 *
 * @since 1.0
 *
 * @param array $meta_boxes
 *
 * @return array
 */
function claudio_register_meta_boxes( $meta_boxes ) {
	// Post format
	$meta_boxes[] = array(
		'id'       => 'post-format-settings',
		'title'    => __( 'Post Format Settings', 'claudio' ),
		'pages'    => array( 'post' ),
		'context'  => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields'   => array(
			array(
				'name'             => __( 'Image', 'claudio' ),
				'id'               => 'image',
				'type'             => 'image_advanced',
				'class'            => 'image',
				'max_file_uploads' => 1,
			),
			array(
				'name'  => __( 'Gallery', 'claudio' ),
				'id'    => 'images',
				'type'  => 'image_advanced',
				'class' => 'gallery',
			),
			array(
				'name'  => __( 'Audio', 'claudio' ),
				'id'    => 'audio',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 2,
				'class' => 'audio',
			),
			array(
				'name'  => __( 'Video', 'claudio' ),
				'id'    => 'video',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 2,
				'class' => 'video',
			),
			array(
				'name'  => __( 'Link', 'claudio' ),
				'id'    => 'url',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'link',
			),
			array(
				'name'  => __( 'Text', 'claudio' ),
				'id'    => 'url_text',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'link',
			),
			array(
				'name'  => __( 'Quote', 'claudio' ),
				'id'    => 'quote',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 2,
				'class' => 'quote',
			),
			array(
				'name'  => __( 'Author', 'claudio' ),
				'id'    => 'quote_author',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'quote',
			),
			array(
				'name'  => __( 'URL', 'claudio' ),
				'id'    => 'author_url',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'quote',
			),
			array(
				'name'  => __( 'Status', 'claudio' ),
				'id'    => 'status',
				'type'  => 'textarea',
				'cols'  => 20,
				'rows'  => 1,
				'class' => 'status',
			),
		),
	);

	// Dispaly Settings
	$meta_boxes[] = array(
		'id'       => 'display-settings',
		'title'    => __( 'Display Settings', 'claudio' ),
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'name' => __( 'Title', 'claudio' ),
				'id'   => 'heading_title',
				'type' => 'heading',
			),
			array(
				'name'  => __( 'Hide The Title', 'claudio' ),
				'id'    => 'hide_title',
				'type'  => 'checkbox',
				'std'   => false,
			),
			array(
				'name' => __( 'Breadcrumb', 'claudio' ),
				'id'   => 'heading_breadcrumb',
				'type' => 'heading',
			),
			array(
				'name'  => __( 'Hide Breadcrumb', 'claudio' ),
				'id'    => 'hide_breadcrumb',
				'type'  => 'checkbox',
				'std'   => false,
			),
			array(
				'name' => __( 'Layout & Styles', 'claudio' ),
				'id'   => 'heading_layout',
				'type' => 'heading',
			),
			array(
				'name'  => __( 'Custom Layout', 'claudio' ),
				'id'    => 'custom_layout',
				'type'  => 'checkbox',
				'std'   => false,
			),
			array(
				'name'            => __( 'Layout', 'claudio' ),
				'id'              => 'layout',
				'type'            => 'image_select',
				'class'           => 'custom-layout',
				'options'         => array(
					'full-content'    => THEME_URL . '/inc/libs/theme-options/img/sidebars/empty.png',
					'sidebar-content' => THEME_URL . '/inc/libs/theme-options/img/sidebars/single-left.png',
					'content-sidebar' => THEME_URL . '/inc/libs/theme-options/img/sidebars/single-right.png',
				),
			),
			array(
				'name'  => __( 'Custom CSS', 'claudio' ),
				'id'    => 'custom_css',
				'type'  => 'textarea',
				'std'   => false,
			),
			array(
				'name'  => __( 'Custom JavaScript', 'claudio' ),
				'id'    => 'custom_js',
				'type'  => 'textarea',
				'std'   => false,
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'claudio_register_meta_boxes' );

/**
 * Enqueue scripts for admin
 *
 * @since  1.0
 */
function claudio_meta_boxes_scripts( $hook ) {
	// Detect to load un-minify scripts when WP_DEBUG is enable
	$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	if ( in_array( $hook, array( 'post.php', 'post-new.php' ) ) ) {
		wp_enqueue_script( 'claudio-meta-boxes', THEME_URL . "/js/backend/meta-boxes$min.js", array( 'jquery' ), THEME_VERSION, true );
	}
}
add_action( 'admin_enqueue_scripts', 'claudio_meta_boxes_scripts' );
