<?php
/**
 * Custom functions for layout.
 *
 * @package Claudio
 */

/**
 * Get layout base on current page
 *
 * @return string
 */
function claudio_get_layout() {
	$layout  = claudio_theme_option( 'default_layout' );

	if ( is_singular() && claudio_get_meta( 'custom_layout' ) ) {
		$layout = claudio_get_meta( 'layout' );
	} elseif ( is_page() ) {
		$layout = claudio_theme_option( 'page_layout' );
	} elseif ( is_singular( 'product' ) ) {
		$layout = 'full-content';
	} elseif ( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
		$layout = claudio_theme_option( 'shop_layout' );
	} elseif ( is_404() ) {
		$layout = 'full-content';
	}

	return $layout;
}

/**
 * Get Bootstrap column classes for content area
 *
 * @since  1.0
 *
 * @return array Array of classes
 */
function claudio_get_content_columns( $layout = null ) {
	$layout = $layout ? $layout : claudio_get_layout();
	if ( 'full-content' == $layout ) {
		return array( 'col-md-12' );
	}

	return array( 'col-md-9', 'col-sm-12', 'col-xs-12' );
}

/**
 * Echos Bootstrap column classes for content area
 *
 * @since 1.0
 */
function claudio_content_columns( $layout = null ) {
	echo implode( ' ', claudio_get_content_columns( $layout ) );
}

/**
 * Get Bootstrap column classes for products
 *
 * @since  1.0
 *
 * @return string
 */
function claudio_wc_content_columns( $columns ) {
	$col = array( 'col-xs-12' );
	if( ! empty( $columns ) ) {
		if ( 5 == $columns ) {
			$col[] = 'col-md-5ths col-sm-3';
		} elseif ( 2 == $columns || 3 == $columns || 4 == $columns ) {

			$column = floor( 12 / $columns );
			$col[] = 'col-sm-' . $column . ' col-md-' . $column;
		}
	}
	$col[] = 'col-product';

	echo implode( ' ', $col );
}
