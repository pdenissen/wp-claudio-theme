<?php
/**
 * Hooks for template nav menus
 *
 * @package Claudio
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 *
 * @since 1.0
 * @param array $args Configuration arguments.
 * @return array
 */
function claudio_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'claudio_page_menu_args' );

/**
 * Add extra items to the end of primary menu
 *
 * @since  1.0.0
 *
 * @param  string $items Items list
 * @param  object $args  Menu options
 *
 * @return string
 */
function claudio_nav_menu_extra_items( $items, $args ) {
	if ( 'primary' != $args->theme_location ) {
		return $items;
	}

	if ( 'logo-center' == claudio_theme_option( 'header_layout' ) ) {
		return $items;
	}

	$extras = claudio_theme_option( 'menu_extra' );
	if ( ! $extras ) {
		return $items;
	}

	foreach ( $extras as $item ) {
		switch ( $item ) {
			case 'cart':
				if ( ! function_exists( 'woocommerce_mini_cart' ) ) {
					break;
				}

				global $woocommerce;

				ob_start();
				woocommerce_mini_cart();
				$mini_cart = ob_get_clean();

				$items .= sprintf(
					'<li class="extra-menu-item menu-item-cart mini-cart woocommerce">
						<a href="%s"><i class="cart-icon"></i></a>
						<div class="widget_shopping_cart_content">%s</div>
					</li>',
					esc_url( $woocommerce->cart->get_cart_url() ),
					$mini_cart
				);
				break;

			case 'search':
				$items .= sprintf(
					'<li class="extra-menu-item menu-item-search">
						<i class="fa fa-search"></i>
						<form class="search-form" method="get" action="%s">
							<input type="text" placeholder="%s" name="s" class="search-field">
							<input type="hidden" name="post_type" value="product">
						</form>
					</li>',
					esc_url( home_url( '/' ) ),
					esc_attr__( 'Search here', 'claudio' )
				);
				break;
		}
	}

	return $items;
}
add_filter( 'wp_nav_menu_items', 'claudio_nav_menu_extra_items', 10, 2 );
