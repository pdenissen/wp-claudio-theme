<?php
/**
 * Custom functions for Visual Composer
 *
 * @package Claudio
 * @subpackage Visual Composer
 */

if ( ! function_exists( 'is_plugin_active' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
}

/**
 * Class Claudio_VC
 *
 * @since 1.0.0
 */
class Claudio_VC {
	/**
	 * List of available icons
	 *
	 * @var array
	 */
	public $icons;

	/**
	 * Construction
	 */
	function __construct() {
		// Stop if VC is not installed
		if ( ! is_plugin_active( 'js_composer/js_composer.php' ) ) {
			return false;
		}

		$this->icons = self::get_icons();

		if ( function_exists( 'vc_add_shortcode_param' ) ) {
			vc_add_shortcode_param( 'select2', array( $this, 'select_param' ), THEME_URL . '/js/vc/select-field.js' );
			vc_add_shortcode_param( 'icon', array( $this, 'icon_param' ), THEME_URL . '/js/vc/icon-field.js' );
		} elseif ( function_exists( 'add_shortcode_param' ) ) {
			add_shortcode_param( 'select2', array( $this, 'select_param' ), THEME_URL . '/js/vc/select-field.js' );
			add_shortcode_param( 'icon', array( $this, 'icon_param' ), THEME_URL . '/js/vc/icon-field.js' );
		} else {
			return false;
		}

		add_action( 'vc_before_init', array( $this, 'map_shortcodes' ) );
	}

	/**
	 * Define icon classes
	 *
	 * @since  1.0.0
	 *
	 * @return array
	 */
	public static function get_icons() {
		$icons = array('fa fa-adjust', 'fa fa-adn', 'fa fa-align-center', 'fa fa-align-justify', 'fa fa-align-left', 'fa fa-align-right', 'fa fa-ambulance', 'fa fa-anchor', 'fa fa-android', 'fa fa-angellist', 'fa fa-angle-double-down', 'fa fa-angle-double-left', 'fa fa-angle-double-right', 'fa fa-angle-double-up', 'fa fa-angle-down', 'fa fa-angle-left', 'fa fa-angle-right', 'fa fa-angle-up', 'fa fa-apple', 'fa fa-archive', 'fa fa-area-chart', 'fa fa-arrow-circle-down', 'fa fa-arrow-circle-left', 'fa fa-arrow-circle-o-down', 'fa fa-arrow-circle-o-left', 'fa fa-arrow-circle-o-right', 'fa fa-arrow-circle-o-up', 'fa fa-arrow-circle-right', 'fa fa-arrow-circle-up', 'fa fa-arrow-down', 'fa fa-arrow-left', 'fa fa-arrow-right', 'fa fa-arrow-up', 'fa fa-arrows', 'fa fa-arrows-alt', 'fa fa-arrows-h', 'fa fa-arrows-v', 'fa fa-asterisk', 'fa fa-at', 'fa fa-automobile', 'fa fa-backward', 'fa fa-ban', 'fa fa-bank', 'fa fa-bar-chart', 'fa fa-bar-chart-o', 'fa fa-barcode', 'fa fa-bars', 'fa fa-beer', 'fa fa-behance', 'fa fa-behance-square', 'fa fa-bell', 'fa fa-bell-o', 'fa fa-bell-slash', 'fa fa-bell-slash-o', 'fa fa-bicycle', 'fa fa-binoculars', 'fa fa-birthday-cake', 'fa fa-bitbucket', 'fa fa-bitbucket-square', 'fa fa-bitcoin', 'fa fa-bold', 'fa fa-bolt', 'fa fa-bomb', 'fa fa-book', 'fa fa-bookmark', 'fa fa-bookmark-o', 'fa fa-briefcase', 'fa fa-btc', 'fa fa-bug', 'fa fa-building', 'fa fa-building-o', 'fa fa-bullhorn', 'fa fa-bullseye', 'fa fa-bus', 'fa fa-cab', 'fa fa-calculator', 'fa fa-calendar', 'fa fa-calendar-o', 'fa fa-camera', 'fa fa-camera-retro', 'fa fa-car', 'fa fa-caret-down', 'fa fa-caret-left', 'fa fa-caret-right', 'fa fa-caret-square-o-down', 'fa fa-caret-square-o-left', 'fa fa-caret-square-o-right', 'fa fa-caret-square-o-up', 'fa fa-caret-up', 'fa fa-cc', 'fa fa-cc-amex', 'fa fa-cc-discover', 'fa fa-cc-mastercard', 'fa fa-cc-paypal', 'fa fa-cc-stripe', 'fa fa-cc-visa', 'fa fa-certificate', 'fa fa-chain', 'fa fa-chain-broken', 'fa fa-check', 'fa fa-check-circle', 'fa fa-check-circle-o', 'fa fa-check-square', 'fa fa-check-square-o', 'fa fa-chevron-circle-down', 'fa fa-chevron-circle-left', 'fa fa-chevron-circle-right', 'fa fa-chevron-circle-up', 'fa fa-chevron-down', 'fa fa-chevron-left', 'fa fa-chevron-right', 'fa fa-chevron-up', 'fa fa-child', 'fa fa-circle', 'fa fa-circle-o', 'fa fa-circle-o-notch', 'fa fa-circle-thin', 'fa fa-clipboard', 'fa fa-clock-o', 'fa fa-close', 'fa fa-cloud', 'fa fa-cloud-download', 'fa fa-cloud-upload', 'fa fa-cny', 'fa fa-code', 'fa fa-code-fork', 'fa fa-codepen', 'fa fa-coffee', 'fa fa-cog', 'fa fa-cogs', 'fa fa-columns', 'fa fa-comment', 'fa fa-comment-o', 'fa fa-comments', 'fa fa-comments-o', 'fa fa-compass', 'fa fa-compress', 'fa fa-copy', 'fa fa-copyright', 'fa fa-credit-card', 'fa fa-crop', 'fa fa-crosshairs', 'fa fa-css3', 'fa fa-cube', 'fa fa-cubes', 'fa fa-cut', 'fa fa-cutlery', 'fa fa-dashboard', 'fa fa-database', 'fa fa-dedent', 'fa fa-delicious', 'fa fa-desktop', 'fa fa-deviantart', 'fa fa-digg', 'fa fa-dollar', 'fa fa-dot-circle-o', 'fa fa-download', 'fa fa-dribbble', 'fa fa-dropbox', 'fa fa-drupal', 'fa fa-edit', 'fa fa-eject', 'fa fa-ellipsis-h', 'fa fa-ellipsis-v', 'fa fa-empire', 'fa fa-envelope', 'fa fa-envelope-o', 'fa fa-envelope-square', 'fa fa-eraser', 'fa fa-eur', 'fa fa-euro', 'fa fa-exchange', 'fa fa-exclamation', 'fa fa-exclamation-circle', 'fa fa-exclamation-triangle', 'fa fa-expand', 'fa fa-external-link', 'fa fa-external-link-square', 'fa fa-eye', 'fa fa-eye-slash', 'fa fa-eyedropper', 'fa fa-facebook', 'fa fa-facebook-square', 'fa fa-fast-backward', 'fa fa-fast-forward', 'fa fa-fax', 'fa fa-female', 'fa fa-fighter-jet', 'fa fa-file', 'fa fa-file-archive-o', 'fa fa-file-audio-o', 'fa fa-file-code-o', 'fa fa-file-excel-o', 'fa fa-file-image-o', 'fa fa-file-movie-o', 'fa fa-file-o', 'fa fa-file-pdf-o', 'fa fa-file-photo-o', 'fa fa-file-picture-o', 'fa fa-file-powerpoint-o', 'fa fa-file-sound-o', 'fa fa-file-text', 'fa fa-file-text-o', 'fa fa-file-video-o', 'fa fa-file-word-o', 'fa fa-file-zip-o', 'fa fa-files-o', 'fa fa-film', 'fa fa-filter', 'fa fa-fire', 'fa fa-fire-extinguisher', 'fa fa-flag', 'fa fa-flag-checkered', 'fa fa-flag-o', 'fa fa-flash', 'fa fa-flask', 'fa fa-flickr', 'fa fa-floppy-o', 'fa fa-folder', 'fa fa-folder-o', 'fa fa-folder-open', 'fa fa-folder-open-o', 'fa fa-font', 'fa fa-forward', 'fa fa-foursquare', 'fa fa-frown-o', 'fa fa-futbol-o', 'fa fa-gamepad', 'fa fa-gavel', 'fa fa-gbp', 'fa fa-ge', 'fa fa-gear', 'fa fa-gears', 'fa fa-gift', 'fa fa-git', 'fa fa-git-square', 'fa fa-github', 'fa fa-github-alt', 'fa fa-github-square', 'fa fa-gittip', 'fa fa-glass', 'fa fa-globe', 'fa fa-google', 'fa fa-google-plus', 'fa fa-google-plus-square', 'fa fa-google-wallet', 'fa fa-graduation-cap', 'fa fa-group', 'fa fa-h-square', 'fa fa-hacker-news', 'fa fa-hand-o-down', 'fa fa-hand-o-left', 'fa fa-hand-o-right', 'fa fa-hand-o-up', 'fa fa-hdd-o', 'fa fa-header', 'fa fa-headphones', 'fa fa-heart', 'fa fa-heart-o', 'fa fa-history', 'fa fa-home', 'fa fa-hospital-o', 'fa fa-html5', 'fa fa-ils', 'fa fa-image', 'fa fa-inbox', 'fa fa-indent', 'fa fa-info', 'fa fa-info-circle', 'fa fa-inr', 'fa fa-instagram', 'fa fa-institution', 'fa fa-ioxhost', 'fa fa-italic', 'fa fa-joomla', 'fa fa-jpy', 'fa fa-jsfiddle', 'fa fa-key', 'fa fa-keyboard-o', 'fa fa-krw', 'fa fa-language', 'fa fa-laptop', 'fa fa-lastfm', 'fa fa-lastfm-square', 'fa fa-leaf', 'fa fa-legal', 'fa fa-lemon-o', 'fa fa-level-down', 'fa fa-level-up', 'fa fa-life-bouy', 'fa fa-life-buoy', 'fa fa-life-ring', 'fa fa-life-saver', 'fa fa-lightbulb-o', 'fa fa-line-chart', 'fa fa-link', 'fa fa-linkedin', 'fa fa-linkedin-square', 'fa fa-linux', 'fa fa-list', 'fa fa-list-alt', 'fa fa-list-ol', 'fa fa-list-ul', 'fa fa-location-arrow', 'fa fa-lock', 'fa fa-long-arrow-down', 'fa fa-long-arrow-left', 'fa fa-long-arrow-right', 'fa fa-long-arrow-up', 'fa fa-magic', 'fa fa-magnet', 'fa fa-mail-forward', 'fa fa-mail-reply', 'fa fa-mail-reply-all', 'fa fa-male', 'fa fa-map-marker', 'fa fa-maxcdn', 'fa fa-meanpath', 'fa fa-medkit', 'fa fa-meh-o', 'fa fa-microphone', 'fa fa-microphone-slash', 'fa fa-minus', 'fa fa-minus-circle', 'fa fa-minus-square', 'fa fa-minus-square-o', 'fa fa-mobile', 'fa fa-mobile-phone', 'fa fa-money', 'fa fa-moon-o', 'fa fa-mortar-board', 'fa fa-music', 'fa fa-navicon', 'fa fa-newspaper-o', 'fa fa-openid', 'fa fa-outdent', 'fa fa-pagelines', 'fa fa-paint-brush', 'fa fa-paper-plane', 'fa fa-paper-plane-o', 'fa fa-paperclip', 'fa fa-paragraph', 'fa fa-paste', 'fa fa-pause', 'fa fa-paw', 'fa fa-paypal', 'fa fa-pencil', 'fa fa-pencil-square', 'fa fa-pencil-square-o', 'fa fa-phone', 'fa fa-phone-square', 'fa fa-photo', 'fa fa-picture-o', 'fa fa-pie-chart', 'fa fa-pied-piper', 'fa fa-pied-piper-alt', 'fa fa-pinterest', 'fa fa-pinterest-square', 'fa fa-plane', 'fa fa-play', 'fa fa-play-circle', 'fa fa-play-circle-o', 'fa fa-plug', 'fa fa-plus', 'fa fa-plus-circle', 'fa fa-plus-square', 'fa fa-plus-square-o', 'fa fa-power-off', 'fa fa-print', 'fa fa-puzzle-piece', 'fa fa-qq', 'fa fa-qrcode', 'fa fa-question', 'fa fa-question-circle', 'fa fa-quote-left', 'fa fa-quote-right', 'fa fa-ra', 'fa fa-random', 'fa fa-rebel', 'fa fa-recycle', 'fa fa-reddit', 'fa fa-reddit-square', 'fa fa-refresh', 'fa fa-remove', 'fa fa-renren', 'fa fa-reorder', 'fa fa-repeat', 'fa fa-reply', 'fa fa-reply-all', 'fa fa-retweet', 'fa fa-rmb', 'fa fa-road', 'fa fa-rocket', 'fa fa-rotate-left', 'fa fa-rotate-right', 'fa fa-rouble', 'fa fa-rss', 'fa fa-rss-square', 'fa fa-rub', 'fa fa-ruble', 'fa fa-rupee', 'fa fa-save', 'fa fa-scissors', 'fa fa-search', 'fa fa-search-minus', 'fa fa-search-plus', 'fa fa-send', 'fa fa-send-o', 'fa fa-share', 'fa fa-share-alt', 'fa fa-share-alt-square', 'fa fa-share-square', 'fa fa-share-square-o', 'fa fa-shekel', 'fa fa-sheqel', 'fa fa-shield', 'fa fa-shopping-cart', 'fa fa-sign-in', 'fa fa-sign-out', 'fa fa-signal', 'fa fa-sitemap', 'fa fa-skype', 'fa fa-slack', 'fa fa-sliders', 'fa fa-slideshare', 'fa fa-smile-o', 'fa fa-soccer-ball-o', 'fa fa-sort', 'fa fa-sort-alpha-asc', 'fa fa-sort-alpha-desc', 'fa fa-sort-amount-asc', 'fa fa-sort-amount-desc', 'fa fa-sort-asc', 'fa fa-sort-desc', 'fa fa-sort-down', 'fa fa-sort-numeric-asc', 'fa fa-sort-numeric-desc', 'fa fa-sort-up', 'fa fa-soundcloud', 'fa fa-space-shuttle', 'fa fa-spinner', 'fa fa-spoon', 'fa fa-spotify', 'fa fa-square', 'fa fa-square-o', 'fa fa-stack-exchange', 'fa fa-stack-overflow', 'fa fa-star', 'fa fa-star-half', 'fa fa-star-half-empty', 'fa fa-star-half-full', 'fa fa-star-half-o', 'fa fa-star-o', 'fa fa-steam', 'fa fa-steam-square', 'fa fa-step-backward', 'fa fa-step-forward', 'fa fa-stethoscope', 'fa fa-stop', 'fa fa-strikethrough', 'fa fa-stumbleupon', 'fa fa-stumbleupon-circle', 'fa fa-subscript', 'fa fa-suitcase', 'fa fa-sun-o', 'fa fa-superscript', 'fa fa-support', 'fa fa-table', 'fa fa-tablet', 'fa fa-tachometer', 'fa fa-tag', 'fa fa-tags', 'fa fa-tasks', 'fa fa-taxi', 'fa fa-tencent-weibo', 'fa fa-terminal', 'fa fa-text-height', 'fa fa-text-width', 'fa fa-th', 'fa fa-th-large', 'fa fa-th-list', 'fa fa-thumb-tack', 'fa fa-thumbs-down', 'fa fa-thumbs-o-down', 'fa fa-thumbs-o-up', 'fa fa-thumbs-up', 'fa fa-ticket', 'fa fa-times', 'fa fa-times-circle', 'fa fa-times-circle-o', 'fa fa-tint', 'fa fa-toggle-down', 'fa fa-toggle-left', 'fa fa-toggle-off', 'fa fa-toggle-on', 'fa fa-toggle-right', 'fa fa-toggle-up', 'fa fa-trash', 'fa fa-trash-o', 'fa fa-tree', 'fa fa-trello', 'fa fa-trophy', 'fa fa-truck', 'fa fa-try', 'fa fa-tty', 'fa fa-tumblr', 'fa fa-tumblr-square', 'fa fa-turkish-lira', 'fa fa-twitch', 'fa fa-twitter', 'fa fa-twitter-square', 'fa fa-umbrella', 'fa fa-underline', 'fa fa-undo', 'fa fa-university', 'fa fa-unlink', 'fa fa-unlock', 'fa fa-unlock-alt', 'fa fa-unsorted', 'fa fa-upload', 'fa fa-usd', 'fa fa-user', 'fa fa-user-md', 'fa fa-users', 'fa fa-video-camera', 'fa fa-vimeo-square', 'fa fa-vine', 'fa fa-vk', 'fa fa-volume-down', 'fa fa-volume-off', 'fa fa-volume-up', 'fa fa-warning', 'fa fa-wechat', 'fa fa-weibo', 'fa fa-weixin', 'fa fa-wheelchair', 'fa fa-wifi', 'fa fa-windows', 'fa fa-won', 'fa fa-wordpress', 'fa fa-wrench', 'fa fa-xing', 'fa fa-xing-square', 'fa fa-yahoo', 'fa fa-yelp', 'fa fa-yen', 'fa fa-youtube', 'fa fa-youtube-play', 'fa fa-youtube-square', );

		return apply_filters( 'claudio_theme_icons', $icons );
	}

	/**
	 * Add new params or add new shortcode to VC
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	function map_shortcodes() {

		// Add attribues to vc_column_inner
		$attributes = array(
			array(
				'type' => 'column_offset',
				'heading' => __('Responsiveness', 'claudio'),
				'param_name' => 'offset',
				'group' => __( 'Width & Responsiveness', 'claudio' ),
				'description' => __('Adjust column for different screen sizes. Control width, offset and visibility settings.', 'claudio')
			)
		);
		vc_add_params( 'vc_column_inner', $attributes );

		$attributes = array(
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Overlay', 'claudio' ),
				'param_name'  => 'overlay',
				'group'       => __( 'Design Options', 'claudio' ),
				'value'       => '',
				'description' => __( 'Select an overlay color for this row', 'claudio' ),
			),
		);

		vc_add_params( 'vc_row', $attributes );

		// Add section title shortcode
		vc_map( array(
			'name'     => __( 'Section Title', 'claudio' ),
			'base'     => 'section_title',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Title', 'claudio' ),
					'param_name'  => 'title',
					'value'       => '',
					'description' => __( 'Enter the title content', 'claudio' ),
				),
				array(
					'type'        => 'textarea_html',
					'holder'      => 'div',
					'heading'     => __( 'Description', 'claudio' ),
					'param_name'  => 'content',
					'value'       => '',
					'description' => __( 'Enter a short description for section', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'value'       => '',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add products carousel shortcode
		vc_map( array(
			'name'     => __( 'Claudio Products Carousel', 'claudio' ),
			'base'     => 'products_carousel',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Products', 'claudio' ),
					'param_name'  => 'products',
					'value'       => array(
						__( 'Recent', 'claudio' )       => 'recent',
						__( 'Featured', 'claudio' )     => 'featured',
						__( 'Best Selling', 'claudio' ) => 'best_selling',
						__( 'Top Rated', 'claudio' )    => 'top_rated',
						__( 'On Sale', 'claudio' )      => 'sale',
					)
				),
				array(
					'type'        => 'select2',
					'holder'      => 'div',
					'heading'     => __( 'Category', 'claudio' ),
					'param_name'  => 'category',
					'description' => __( 'Select a category or select All to get products from all categories.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Total Products', 'claudio' ),
					'param_name'  => 'per_page',
					'value'       => '12',
					'description' => __( 'Set numbers of products to show.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Columns', 'claudio' ),
					'param_name'  => 'columns',
					'value'       => array(
						__( '4 columns', 'claudio' ) => '4',
						__( '2 columns', 'claudio' ) => '2',
						__( '3 columns', 'claudio' ) => '3',
						__( '5 columns', 'claudio' ) => '5',
					),
					'description' => __( 'Select numbers of columns you want to display.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order By', 'claudio' ),
					'param_name'  => 'orderby',
					'value'       => array(
						''                            => '',
						__( 'Date', 'claudio' )       => 'date',
						__( 'Title', 'claudio' )      => 'title',
						__( 'Menu Order', 'claudio' ) => 'menu_order',
						__( 'Random', 'claudio' )     => 'rand',
					),
					'dependency'  => array( 'element' => 'products', 'value' => array( 'top_rated', 'sale', 'featured' ) ),
					'description' => __( 'Select to order products. Leave empty to use the default order by of theme.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order', 'claudio' ),
					'param_name'  => 'order',
					'value'       => array(
						''                             => '',
						__( 'Ascending ', 'claudio' )  => 'asc',
						__( 'Descending ', 'claudio' ) => 'desc',
					),
					'dependency'  => array( 'element' => 'products', 'value' => array( 'top_rated', 'sale', 'featured' ) ),
					'description' => __( 'Select to sort products. Leave empty to use the default sort of theme', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Slider autoplay', 'claudio' ),
					'param_name'  => 'auto_play',
					'value'       => array( __( 'Yes', 'claudio' ) => 'true' ),
					'description' => __( 'Enables autoplay mode.', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Hide prev/next buttons', 'claudio' ),
					'param_name'  => 'hide_navigation',
					'value'       => array( __( 'Yes', 'claudio' ) => 'false' ),
					'description' => __( 'If "YES" prev/next control will be removed.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'value'       => '',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add products shortcode
		vc_map( array(
			'name'     => __( 'Claudio Products', 'claudio' ),
			'base'     => 'claudio_products',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Products', 'claudio' ),
					'param_name'  => 'products',
					'value'       => array(
						__( 'Recent', 'claudio' )       => 'recent',
						__( 'Featured', 'claudio' )     => 'featured',
						__( 'Best Selling', 'claudio' ) => 'best_selling',
						__( 'Top Rated', 'claudio' )    => 'top_rated',
						__( 'On Sale', 'claudio' )      => 'sale',
					)
				),
				array(
					'type'        => 'select2',
					'holder'      => 'div',
					'heading'     => __( 'Category', 'claudio' ),
					'param_name'  => 'category',
					'description' => __( 'Select a category or select All to get products from all categories.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Number of products', 'claudio' ),
					'param_name'  => 'per_page',
					'value'       => '4',
					'description' => __( 'Set number of products to show.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Columns', 'claudio' ),
					'param_name'  => 'columns',
					'value'       => array(
						__( '4 columns', 'claudio' ) => '4',
						__( '2 columns', 'claudio' ) => '2',
						__( '3 columns', 'claudio' ) => '3',
						__( '5 columns', 'claudio' ) => '5',
					),
					'description' => __( "Select numbers of columns you want to display.", 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order By', 'claudio' ),
					'param_name'  => 'orderby',
					'value'       => array(
						''                            => '',
						__( 'Date', 'claudio' )       => 'date',
						__( 'Title', 'claudio' )      => 'title',
						__( 'Menu Order', 'claudio' ) => 'menu_order',
						__( 'Random', 'claudio' )     => 'rand',
					),
					'dependency'  => array( 'element' => 'products', 'value' => array( 'top_rated', 'sale', 'featured' ) ),
					'description' => __( 'Select to order products. Leave empty to use the default order by of theme.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order', 'claudio' ),
					'param_name'  => 'order',
					'value'       => array(
						''                             => '',
						__( 'Ascending ', 'claudio' )  => 'asc',
						__( 'Descending ', 'claudio' ) => 'desc',
					),
					'dependency'  => array( 'element' => 'products', 'value' => array( 'top_rated', 'sale', 'featured' ) ),
					'description' => __( 'Select to sort products. Leave empty to use the default sort of theme', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'View More Link', 'claudio' ),
					'param_name'  => 'view_more_link',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'View More Text', 'claudio' ),
					'param_name'  => 'view_more_text',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'value'       => '',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add products ajax shortcode
		vc_map( array(
			'name'     => __( 'Claudio Products Ajax', 'claudio' ),
			'base'     => 'products_ajax',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Products', 'claudio' ),
					'param_name'  => 'products',
					'value'       => array(
						__( 'Recent', 'claudio' )       => 'recent',
						__( 'Featured', 'claudio' )     => 'featured',
						__( 'Best Selling', 'claudio' ) => 'best_selling',
						__( 'Top Rated', 'claudio' )    => 'top_rated',
						__( 'On Sale', 'claudio' )      => 'sale',
					)
				),
				array(
					'type'        => 'select2',
					'holder'      => 'div',
					'heading'     => __( 'Category', 'claudio' ),
					'param_name'  => 'category',
					'description' => __( 'Select a category or select All to get products from all categories.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Number of products', 'claudio' ),
					'param_name'  => 'per_page',
					'value'       => '9',
					'description' => __( 'Set numbers of products to show.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Columns', 'claudio' ),
					'param_name'  => 'columns',
					'value'       => array(
						__( '3 columns', 'claudio' ) => '3',
						__( '4 columns', 'claudio' ) => '4',
						__( '5 columns', 'claudio' ) => '5',
					),
					'description' => __( 'Select numbers of columns you want to display.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order By', 'claudio' ),
					'param_name'  => 'orderby',
					'value'       => array(
						''                            => '',
						__( 'Date', 'claudio' )       => 'date',
						__( 'Title', 'claudio' )      => 'title',
						__( 'Menu Order', 'claudio' ) => 'menu_order',
						__( 'Random', 'claudio' )     => 'rand',
					),
					'dependency'  => array( 'element' => 'products', 'value' => array( 'top_rated', 'sale', 'featured' ) ),
					'description' => __( 'Select to order products. Leave empty to use the default order by of theme.', 'claudio' ),
				),

				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order', 'claudio' ),
					'param_name'  => 'order',
					'value'       => array(
						''                             => '',
						__( 'Ascending ', 'claudio' )  => 'asc',
						__( 'Descending ', 'claudio' ) => 'desc',
					),
					'dependency'  => array( 'element' => 'products', 'value' => array( 'top_rated', 'sale', 'featured' ) ),
					'description' => __( 'Select to sort products. Leave empty to use the default sort of theme', 'claudio' ),
				),
				array(
					'type'       => 'textfield',
					'holder'     => 'div',
					'heading'    => __( 'View More Text', 'claudio' ),
					'param_name' => 'view_more_text',
					'value'      => __( 'View More Products', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'value'       => '',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );


		// Add images carousel shortcode
		vc_map( array(
			'name'     => __( 'Claudio Images Carousel', 'claudio' ),
			'base'     => 'images_carousel',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'attach_images',
					'holder'      => 'div',
					'heading'     => __( 'Images', 'claudio' ),
					'param_name'  => 'images',
					'value'       => '',
					'description' => __( 'Select images from media library', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Image size', 'claudio' ),
					'param_name'  => 'image_size',
					'value'       => $this->image_sizes(),
					'description' => __( 'Select image size. Leave empty to use "thumbnail" size.', 'claudio' ),
				),
				array(
					'type'        => 'textarea',
					'holder'      => 'div',
					'heading'     => __( 'Custom links', 'claudio' ),
					'param_name'  => 'custom_links',
					'description' => __( 'Enter links for each slide here. Divide links with linebreaks (Enter).', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Custom link target', 'claudio' ),
					'param_name'  => 'custom_links_target',
					'value'       => array(
						__( 'Same window', 'claudio' ) => '_self',
						__( 'New window', 'claudio' )  => '_blank',
					),
					'description' => __( 'Select where to open custom links.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Slides per view', 'claudio' ),
					'param_name'  => 'number',
					'value'       => 4,
					'description' => __( 'Set numbers of slides you want to display at the same time on slider\'s container for carousel mode.', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Slider autoplay', 'claudio' ),
					'param_name'  => 'auto_play',
					'value'       => array( __( 'Yes', 'claudio' ) => 'true' ),
					'description' => __( 'Enables autoplay mode.', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Hide prev/next buttons', 'claudio' ),
					'param_name'  => 'hide_navigation',
					'value'       => array( __( 'Yes', 'claudio' ) => 'true' ),
					'description' => __( 'If "YES" prev/next control will be removed.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add posts carousel shortcode
		vc_map( array(
			'name'     => __( 'Claudio Posts Carousel', 'claudio' ),
			'base'     => 'posts_carousel',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Total Posts', 'claudio' ),
					'param_name'  => 'total',
					'value'       => '6',
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Category', 'claudio' ),
					'param_name'  => 'categories',
					'value'       => $this->get_categories(),
					'description' => __( 'Select a category or all categories.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order By', 'claudio' ),
					'param_name'  => 'order_by',
					'value'       => array(
						__( 'Date', 'claudio' )       => 'date',
						__( 'Title', 'claudio' )      => 'title',
						__( 'Modified', 'claudio' )   => 'modified',
						__( 'Menu Order', 'claudio' ) => 'menu_order',
						__( 'Random', 'claudio' )     => 'rand',
					),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Order', 'claudio' ),
					'param_name'  => 'order',
					'value'       => array(
						__( 'Descending ', 'claudio' ) => 'desc',
						__( 'Ascending ', 'claudio' )  => 'asc',
					),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Excerpt Length (words)', 'claudio' ),
					'param_name'  => 'excerpt_length',
					'value'       => 15,
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Readmore Text', 'claudio' ),
					'param_name'  => 'read_more_text',
					'value'       => __( 'Read More', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Posts per view', 'claudio' ),
					'param_name'  => 'number',
					'value'       => 2,
					'description' => __( 'Set numbers of slides you want to display at the same time on slider\'s container for carousel mode.', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Slider autoplay', 'claudio' ),
					'param_name'  => 'auto_play',
					'value'       => array( __( 'Yes', 'claudio' ) => 'true' ),
					'description' => __( 'Enables autoplay mode.', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Hide prev/next buttons', 'claudio' ),
					'param_name'  => 'hide_navigation',
					'value'       => array( __( 'Yes', 'claudio' ) => 'true' ),
					'description' => __( 'If "YES" prev/next control will be removed.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Promotion Image shortcode
		vc_map( array(
			'name'     => __( 'Promotion Image', 'claudio' ),
			'base'     => 'promotion_image',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Title', 'claudio' ),
					'param_name'  => 'content',
					'value'       => '',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Subtitle', 'claudio' ),
					'param_name'  => 'subtitle',
					'value'       => '',
				),
				array(
					'type'        => 'attach_image',
					'holder'      => 'div',
					'heading'     => __( 'Image', 'claudio' ),
					'param_name'  => 'image',
					'value'       => '',
					'description' => __( 'Select image from media library', 'claudio' ),
				),
				array(
					'type'        => 'colorpicker',
					'heading'     => __( 'Background Overlay', 'claudio' ),
					'param_name'  => 'overlay',
					'value'       => '',
					'description' => __( 'Select an overlay color for this element', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Link', 'claudio' ),
					'param_name'  => 'link',
					'description' => __( 'Enter link for this element here.', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Custom link target', 'claudio' ),
					'param_name'  => 'custom_link_target',
					'value'       => array(
						__( 'Same window', 'claudio' ) => '_self',
						__( 'New window', 'claudio' )  => '_blank',
					),
					'description' => __( 'Select where to open custom link.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add instagram shortcode
		vc_map( array(
			'name'     => __( 'Instagram Photos', 'claudio' ),
			'base'     => 'instagram',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'User Name', 'claudio' ),
					'param_name'  => 'user_id',
					'description' => __( 'Username of account you want to get feed', 'claudio' ),
				),
				// array(
				// 	'type'        => 'textfield',
				// 	'holder'      => 'div',
				// 	'heading'     => __( 'Access Token', 'claudio' ),
				// 	'param_name'  => 'access_token',
				// 	'description' => __( '<a href="https://smashballoon.com/instagram-feed/token/">Get your Access Token and User ID</a>', 'claudio' ),
				// ),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Numbers', 'claudio' ),
					'param_name'  => 'numbers',
					'value'       => 8,
					'description' => __( 'Enter number of photos you want to show.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Banner shortcode
		vc_map( array(
			'name'     => __( 'Banner', 'claudio' ),
			'base'     => 'banner',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'attach_image',
					'holder'      => 'div',
					'heading'     => __( 'Background Image', 'claudio' ),
					'param_name'  => 'image',
					'value'       => '',
					'description' => __( 'Select image from media library', 'claudio' ),
				),
				array(
					'type'        => 'colorpicker',
					'heading'     => __( 'Background Overlay', 'claudio' ),
					'param_name'  => 'overlay',
					'value'       => '',
					'description' => __( 'Select an overlay color for this element', 'claudio' ),
				),
				array(
					'type'        => 'textarea_html',
					'holder'      => 'div',
					'heading'     => __( 'Title', 'claudio' ),
					'param_name'  => 'content',
					'value'       => '',
					'description' => __( 'Enter the title content', 'claudio' ),
				),
				array(
					'type'        => 'textarea',
					'holder'      => 'div',
					'heading'     => __( 'Description', 'claudio' ),
					'param_name'  => 'desc',
					'value'       => '',
					'description' => __( 'Enter a short description for section', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Button Text', 'claudio' ),
					'param_name'  => 'button_text',
					'value'       => '',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Button Link', 'claudio' ),
					'param_name'  => 'button_link',
					'value'       => '',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Footer Text', 'claudio' ),
					'param_name'  => 'footer_text',
					'value'       => '',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Footer Link', 'claudio' ),
					'param_name'  => 'footer_link',
					'value'       => '',
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Text Alignment', 'claudio' ),
					'param_name'  => 'text_align',
					'value'       => array(
						__( 'Align Center', 'claudio' ) => 'align-center',
						__( 'Align Left', 'claudio' )   => 'align-left',
						__( 'Align Right', 'claudio' )  => 'align-right',
					),
					'description' => __( 'Select text alignment for this element.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'value'       => '',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add newsletter shortcode
		vc_map( array(
			'name'     => __( 'Newsletter Form', 'claudio' ),
			'base'     => 'newsletter_form',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'params'   => array(
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Style', 'claudio' ),
					'param_name'  => 'style',
					'value'       => array(
						__( 'Style 1', 'claudio' ) => '',
						__( 'Style 2', 'claudio' ) => 'style-2',
					),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Featured Category shortcode
		vc_map( array(
			'name'              => __( 'Featured Category', 'claudio' ),
			'base'              => 'featured_category',
			'category'          => __( 'Content', 'claudio' ),
			'admin_enqueue_js'  => THEME_URL . '/js/vc/select2.min.js',
			'admin_enqueue_css' => THEME_URL . '/css/vc/select2.css',
			'params'            => array(
				array(
					'type'        => 'select2',
					'holder'      => 'div',
					'heading'     => __( 'Category','claudio' ),
					'param_name'  => 'category',
					'value'       => '',
					'description' => __( 'Select a category.', 'claudio' ),
				),
				array(
					'type'        => 'attach_image',
					'holder'      => 'div',
					'heading'     => __( 'Image', 'claudio' ),
					'param_name'  => 'image',
					'value'       => '',
					'description' => __( 'Select image from media library', 'claudio' ),
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Image size', 'claudio' ),
					'param_name'  => 'image_size',
					'value'       => $this->image_sizes(),
					'description' => __( 'Select image size. Leave empty to use "thumbnail" size.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Video Banner
		vc_map( array(
			'name'              => __( 'Video Banner', 'claudio' ),
			'base'              => 'video_banner',
			'category'          => __( 'Content', 'claudio' ),
			'params'            => array(
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Video file URL','claudio' ),
					'description' => __( 'Only allow mp4, webm, ogv files', 'claudio' ),
					'param_name'  => 'video',
					'value'       => '',
				),
				array(
					'type'        => 'attach_image',
					'holder'      => 'div',
					'heading'     => __( 'Poster Image', 'claudio' ),
					'param_name'  => 'image',
					'value'       => '',
					'description' => __( 'Select poster image to display at the beginning.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Height', 'claudio' ),
					'param_name'  => 'height',
					'value'       => '',
					'description' => __( 'Specify height of video banner or leave it empty to use the height of video', 'claudio' ),
				),
				array(
					'type'        => 'checkbox',
					'holder'      => 'div',
					'heading'     => __( 'Mute', 'claudio' ),
					'param_name'  => 'mute',
					'value'       => array( __( 'Yes', 'claudio' ) => 'true' ),
					'description' => __( 'Mute this video by default', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				),
			),
		) );

		// Add Icon Box shortcode
		vc_map( array(
			'name'     => __( 'Icon Box', 'claudio' ),
			'base'     => 'icon_box',
			'class'    => '',
			'category' => __( 'Content', 'claudio' ),
			'admin_enqueue_css' => THEME_URL . '/css/vc/icon-field.css',
			'params'   => array(
				array(
					'type'        => 'icon',
					'holder'      => 'div',
					'heading'     => __( 'Icon', 'claudio' ),
					'param_name'  => 'icon',
					'value'       => '',
				),
				array(
					'type'        => 'dropdown',
					'holder'      => 'div',
					'heading'     => __( 'Icon Position', 'claudio' ),
					'param_name'  => 'icon_position',
					'value'       => array(
						__( 'Left', 'claudio' ) => 'Left',
						__( 'Top', 'claudio' )  => 'top',
					),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Title', 'claudio' ),
					'param_name'  => 'title',
					'value'       => '',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'SubTitle', 'claudio' ),
					'param_name'  => 'subtitle',
					'value'       => '',
				),
				array(
					'type'        => 'textarea_html',
					'holder'      => 'div',
					'heading'     => __( 'Content', 'claudio' ),
					'param_name'  => 'content',
					'value'       => '',
					'description' => __( 'Enter the content of this box', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Read More Text', 'claudio' ),
					'param_name'  => 'read_more_text',
					'value'       => '',
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Link', 'claudio' ),
					'param_name'  => 'link',
					'value'       => '',
					'description' => __( 'Enter URL if you want this title to have a link.', 'claudio' ),
				),
				array(
					'type'        => 'textfield',
					'holder'      => 'div',
					'heading'     => __( 'Extra class name', 'claudio' ),
					'param_name'  => 'class_name',
					'value'       => '',
					'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'claudio' ),
				)
			),
		) );
	}

	/**
	 * Get available image sizes
	 *
	 * @return string
	 */
	function image_sizes() {
		$output = array();
		$output[__( 'Full Size', 'claudio' )] = 'full';
		foreach ( $this->get_image_sizes() as $name => $size ) {
			$output[ucfirst( $name ) . ' (' . $size['width'] . 'x' . $size['height'] . ')'] = $name;
		}
		return $output;
	}

	/**
	 * Get available image sizes with width and height following
	 *
	 * @return array|bool
	 */
	function get_image_sizes() {
		global $_wp_additional_image_sizes;

		$sizes       = array();
		$image_sizes = get_intermediate_image_sizes();

		// Create the full array with sizes and crop info
		foreach ( $image_sizes as $size ) {
			if ( in_array( $size, array( 'thumbnail', 'medium', 'large' ) ) ) {
				$sizes[$size]['width']  = get_option( $size . '_size_w' );
				$sizes[$size]['height'] = get_option( $size . '_size_h' );
			} elseif ( isset( $_wp_additional_image_sizes[$size] ) ) {
				$sizes[$size] = array(
					'width'  => $_wp_additional_image_sizes[$size]['width'],
					'height' => $_wp_additional_image_sizes[$size]['height'],
				);
			}
		}

		return $sizes;
	}

	/**
	 * Get categories
	 *
	 * @return array|string
	 */
	function get_categories( $taxonomy = 'category' ) {
		$output[__( 'All', 'claudio' )] = '';
		$categories = get_terms( $taxonomy );
		if( $categories  ) {
			foreach ( $categories as $category ) {
				$output[$category->name] = $category->slug;
			}
		}
		return $output;
	}

	/**
	 * Get MailPoet Form
	 *
	 * @return array|string
	 */
	function get_mailpoet_forms() {
		if( ! class_exists( 'WYSIJA' ) ) {
			return '';
		}

		$model_forms = WYSIJA::get( 'forms', 'model' );
		$model_forms->reset();
		$forms = $model_forms->getRows( array( 'form_id', 'name' ) );

		$output = array();
		$output[__( 'Select a form', 'claudio' )] = '0';
		if( $forms ) {
			foreach ( $forms as $form ) {
				$output[$form['name']] = $form['form_id'];
			}
		}
		return $output;
	}

	/**
	 * Return setting UI for icon param type
	 *
	 * @param  array $settings
	 * @param  string $value
	 *
	 * @return string
	 */
	function select_param( $settings, $value ) {
		// Generate dependencies if there are any
		$dependency = vc_generate_dependencies_attributes( $settings );
		$categories = get_terms( 'product_cat' );
		$cat = array();
		$cat[] = sprintf( '<option value="%s">%s</option>', '', __( 'All', 'claudio' ) );
		foreach( $categories as $category ) {
			if( $category ) {
				$cat[] = sprintf('<option value="%s">%s</option>',
					esc_attr( $category->slug ),
					$category->name
				);
			}

		}

		return sprintf(
			'<input type="hidden" name="%s" value="%s" class="wpb-input-categories wpb_vc_param_value wpb-textinput %s %s_field" %s>
			<select class="select-categories-post">
			%s
			</select>',
			esc_attr( $settings['param_name'] ),
			esc_attr( $value ),
			esc_attr( $settings['param_name'] ),
			esc_attr( $settings['type'] ),
			$dependency,
			implode( '', $cat )
		);
	}

	/**
	 * Return setting UI for icon param type
	 *
	 * @param  array $settings
	 * @param  string $value
	 *
	 * @return string
	 */
	function icon_param( $settings, $value ) {
		// Generate dependencies if there are any
		$dependency = vc_generate_dependencies_attributes( $settings );
		$icons = array();
		foreach( $this->icons as $icon ) {
			$icons[] = sprintf(
				'<i data-icon="%1$s" class="%1$s %2$s"></i>',
				$icon,
				$icon == $value ? 'selected' : ''
			);
		}

		return sprintf(
			'<div class="icon_block">
				<span class="icon-preview"><i class="%s"></i></span>
				<input type="text" class="icon-search" placeholder="%s">
				<input type="hidden" name="%s" value="%s" class="wpb_vc_param_value wpb-textinput %s %s_field" %s>
				<div class="icon-selector">%s</div>
			</div>',
			esc_attr( $value ),
			esc_attr__( 'Quick Search', 'claudio' ),
			esc_attr( $settings['param_name'] ),
			esc_attr( $value ),
			esc_attr( $settings['param_name'] ),
			esc_attr( $settings['type'] ),
			$dependency,
			implode( '', $icons )
		);
	}
}
