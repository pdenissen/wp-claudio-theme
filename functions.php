<?php
/**
 * Theme Alien Core functions and definitions
 *
 * @package Claudio
 */

/**
 * Define theme's constant
 */
if ( ! defined( 'THEME_VERSION' ) ) {
	define( 'THEME_VERSION', '1.1.6' );
}
if ( ! defined( 'THEME_DIR' ) ) {
	define( 'THEME_DIR', get_template_directory() );
}
if ( ! defined( 'THEME_URL' ) ) {
	define( 'THEME_URL', get_template_directory_uri() );
}

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * @since  1.0
 *
 * @return void
 */
function claudio_setup() {
	// Make theme available for translation.
	load_theme_textdomain( 'claudio', get_template_directory() . '/lang' );

	// Theme supports
	add_theme_support( 'woocommerce' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );

	// Register theme nav menu
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'claudio' ),
		'footer'  => __( 'Footer Menu', 'claudio' ),
		'mobile'  => __( 'Mobile Menu', 'claudio' ),
	) );

	// Register new image sizes
	add_image_size( 'claudio-category-thumb', 370, 450, true );
	add_image_size( 'claudio-blog-small-thumb', 170, 170, true );
	add_image_size( 'claudio-blog-thumb', 870, 385, true );
	add_image_size( 'claudio-blog-large-thumb', 1170, 520, true );
	add_image_size( 'claudio-widget-thumb', 60, 60, true );

	// Initialize
	new Claudio_VC;
	new Claudio_WooCommerce;

	if ( is_admin() ) {
		new Claudio_Walker_Nav_Menu_Custom_Fields;
	} else {
		new Claudio_Shortcodes;
	}
}
add_action( 'after_setup_theme', 'claudio_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 *
 * @since 1.0
 *
 * @return void
 */
function claudio_register_sidebars() {
	$sidebars = array(
		'blog-sidebar' => __( 'Blog Sidebar', 'claudio' ),
		'shop-sidebar' => __( 'Shop Sidebar', 'claudio' ),
		'page-sidebar' => __( 'Page Sidebar', 'claudio' ),
		'topbar-left'  => __( 'Topbar Left', 'claudio' ),
		'topbar-right' => __( 'Topbar Right', 'claudio' ),
	);

	// Register sidebars
	foreach( $sidebars as $id => $name ) {
		register_sidebar( array(
			'name'          => $name,
			'id'            => $id,
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}

	// Register footer sidebars
	for ( $i = 1; $i <= 5; $i++ ) {
		register_sidebar( array(
			'name'          => __( 'Footer', 'claudio' ) . " $i",
			'id'            => "footer-sidebar-$i",
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}
}
add_action( 'widgets_init', 'claudio_register_sidebars' );

/**
 * Load theme
 */

// Theme Options
require THEME_DIR . '/inc/libs/theme-options/framework.php';
require THEME_DIR . '/inc/backend/theme-options.php';

// Widgets
require THEME_DIR . '/inc/widgets/widgets.php';

// Visual composer
require THEME_DIR . '/inc/backend/visual-composer.php';

// Woocommerce hooks
require THEME_DIR . '/inc/frontend/woocommerce.php';

if ( is_admin() ) {
	require THEME_DIR . '/inc/libs/class-tgm-plugin-activation.php';
	require THEME_DIR . '/inc/backend/plugins.php';
	require THEME_DIR . '/inc/backend/meta-boxes.php';
	require THEME_DIR . '/inc/backend/nav-menus.php';
} else {
	// Frontend functions and shortcodes
	require THEME_DIR . '/inc/functions/breadcrumbs.php';
	require THEME_DIR . '/inc/functions/media.php';
	require THEME_DIR . '/inc/functions/nav.php';
	require THEME_DIR . '/inc/functions/layout.php';
	require THEME_DIR . '/inc/functions/entry.php';
	require THEME_DIR . '/inc/functions/shortcodes.php';
	require THEME_DIR . '/inc/functions/menu-walker.php';

	// Frontend hooks
	require THEME_DIR . '/inc/frontend/layout.php';
	require THEME_DIR . '/inc/frontend/header.php';
	require THEME_DIR . '/inc/frontend/nav.php';
	require THEME_DIR . '/inc/frontend/entry.php';
	require THEME_DIR . '/inc/frontend/footer.php';
}
