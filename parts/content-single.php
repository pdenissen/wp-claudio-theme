<?php
/**
 * The template used for displaying post content in single.php
 *
 * @package Claudio
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if(  ! claudio_get_meta( 'hide_singular_title' ) ) : ?>
		<header class="entry-header clearfix">
			<?php claudio_entry_thumbnail(); ?>

			<h2 class="entry-title"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title() ?></a></h2>

			<div class="entry-info">
				<span class="entry-author">
					<i class="fa fa-pencil"></i>
					<?php _e( 'By:', 'claudio' ); ?>
					<a class="url fn n" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>" title="<?php echo esc_attr( sprintf( __( 'View all posts by %s', 'claudio' ), get_the_author() ) ) ?>" rel="author">
						<?php the_author(); ?>
					</a>
				</span>
				<?php
				$time_string = '<time class="entry-date published" datetime="%s"><i class="fa fa-calendar"></i><span>%s</span></time>';

				printf(
					$time_string,
					esc_attr( get_the_date( 'c' ) ),
					esc_html( get_the_date( 'd F Y' ) )
				);
				?>

				<span class="entry-comments">
					<i class="fa fa-comments-o"></i>
					<?php comments_popup_link( __( '0 comment', 'claudio' ), __( '1 comments', 'claudio' ), __( '% comments', 'claudio' ), 'comments-link' ); ?>
				</span>
			</div>
		</header>
	<?php endif; ?>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'claudio' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php if ( has_category() ) : ?>
			<span class="category-links"><strong><?php _e( 'Category', 'claudio' ) ?>:</strong> <?php the_category( ', ' ) ?></span>
		<?php endif; ?>
		<?php the_tags( '<span class="tags-links"><strong>' . __( 'Tags', 'claudio' ) . ':</strong> ', ', ', '</span>' ) ?>
	</footer>
</article><!-- #post-## -->
