<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Claudio
 */

get_header(); ?>

	<section id="primary" class="content-area <?php claudio_content_columns() ?>">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'claudio' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */

				if( get_post_type() == 'product' ) {
					echo '<ul class="products">';
					get_template_part( 'woocommerce/content', 'product' );
					echo '</ul>';
				} else {
					get_template_part( 'parts/content', 'search' );
				}
				?>

			<?php endwhile; ?>

			<div class="pagination">
				<?php
				claudio_results_pagination();
				claudio_numeric_pagination();
				?>
			</div>

		<?php else : ?>

			<?php get_template_part( 'parts/content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
