<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Claudio
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!--[if lt IE 9]>
	<script src="//cdn.jsdelivr.net/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="//cdn.jsdelivr.net/respond/1.3.0/respond.min.js"></script>
	<script>
	window.html5 || document.write('<script src="<?php echo esc_url( THEME_URL ) ?>/js/html5shiv.js"><\/script>');
	window.respond || document.write('<script src="<?php echo esc_url( THEME_URL ) ?>/js/respond.js"><\/script>');
	</script>
	<![endif]-->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">

	<?php do_action( 'claudio_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner">
		<?php do_action( 'claudio_header' ); ?>
	</header><!-- #masthead -->

	<?php do_action( 'claudio_after_header' ); ?>

	<div id="content" class="site-content">
		<?php if ( ! is_page_template( 'template-full-width.php' ) ) : ?>
			<div class="container">
				<div class="row">
		<?php endif; ?>
