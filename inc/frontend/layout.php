<?php
/**
 * Hooks for frontend display
 *
 * @package Claudio
 */



/**
 * Adds custom classes to the array of body classes.
 *
 * @since 1.0
 * @param array $classes Classes for the body element.
 * @return array
 */
function claudio_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Add a class of layout style
	$classes[] = claudio_theme_option( 'layout_style' );

	// Add a class of layout
	$classes[] = claudio_get_layout();

	// Add a class of shop view
	if ( function_exists( 'is_shop' ) && ( is_shop() || is_product_category() || is_product_tag() ) ) {
		$view = isset( $_COOKIE['shop_view'] ) ? $_COOKIE['shop_view'] : claudio_theme_option( 'shop_view' );
		$classes[] = 'shop-view-' . $view;
	}

	// Add a class of header layout
	$classes[] = claudio_theme_option( 'header_layout' );

	// Add a class of header color
	$classes[] = 'header-' . claudio_theme_option( 'header_color' );

	// Add a class of footer layout
	$classes[] = 'footer-' . claudio_theme_option( 'footer_layout' );

	// Add a class of footer color
	$classes[] = 'footer-' . claudio_theme_option( 'footer_color' );

	// Add a class for color scheme
	if ( intval( claudio_theme_option( 'custom_color_scheme' ) ) && claudio_theme_option( 'custom_color_1' ) ) {
		$classes[] = 'custom-color-scheme';
	} else {
		$classes[] = claudio_theme_option( 'color_scheme' );
	}

	if( is_search() ) {
		$classes[] = 'woocommerce shop-view-list';
	}

	return $classes;
}
add_filter( 'body_class', 'claudio_body_classes' );
