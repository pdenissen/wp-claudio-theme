<?php

/**
 * Class for all WooCommerce template modification
 *
 * @version 1.0
 */
class Claudio_WooCommerce {
	/**
	 * @var string Layout of current page
	 */
	public $layout;

	/**
	 * Construction function
	 *
	 * @since  1.0
	 * @return claudio_WooCommerce
	 */
	function __construct() {
		// Check if Woocomerce plugin is actived
		if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			return false;
		}

		// Define all hook
		add_action( 'template_redirect', array( $this, 'hooks' ) );

		// Need an early hook to ajaxify update mini shop cart
		add_filter( 'add_to_cart_fragments', array( $this, 'add_to_cart_fragments' ) );
	}

	/**
	 * Hooks to WooCommerce actions, filters
	 *
	 * @since  1.0
	 * @return void
	 */
	function hooks() {
		$this->layout       = claudio_get_layout();
		$this->new_duration = claudio_theme_option( 'product_newness' );
		$this->shop_view    = isset( $_COOKIE['shop_view'] ) ? $_COOKIE['shop_view'] : claudio_theme_option( 'shop_view' );

		// WooCommerce Styles
		add_filter( 'woocommerce_enqueue_styles', array( $this, 'wc_styles' ) );

		// Remove breadcrumb, use theme's instead
		remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

		// Add toolbars for shop page
		add_filter( 'woocommerce_show_page_title', '__return_false' );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
		remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
		add_action( 'woocommerce_before_shop_loop', array( $this, 'shop_toolbar' ) );

		// Change shop columns
		add_filter( 'loop_shop_columns', array( $this, 'shop_columns' ), 50 );

		// Remove product link
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

		// Add Bootstrap classes
		add_filter( 'post_class', array( $this, 'product_class' ), 50, 3 );

		// Wrap product loop content
		add_action( 'woocommerce_before_shop_loop_item', array( $this, 'open_product_inner' ), 1 );
		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'close_product_inner' ), 100 );

		// Wrap product info
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'open_product_info' ), 50 );
		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'close_product_info' ), 90 );

		// Add badges
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash' );
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'product_ribbons' ) );
		add_action( 'woocommerce_before_single_product_summary', array( $this, 'product_ribbons' ) );

		// Add secondary image to product thumbnail
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail' );
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'product_content_thumbnail' ) );

		// Display product excerpt and subcategory description for list view
		add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 5 );
		add_action( 'woocommerce_after_subcategory', array( $this, 'show_cat_desc' ) );

		// Add product categories before the title
		add_action( 'woocommerce_shop_loop_item_title', array( $this, 'product_categories' ) );

		// Re-markup product title in loop
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title' );
		add_action( 'woocommerce_shop_loop_item_title', array( $this, 'product_title' ) );

		// Change number of related products
		add_filter( 'woocommerce_output_related_products_args', array( $this, 'related_products_args' ) );
		add_filter( 'woocommerce_cross_sells_columns', array( $this, 'cross_sells_columns' ) );

		// Change columns number of product thumbnails in the single product page
		add_filter( 'woocommerce_product_thumbnails_columns', array( $this, 'product_thumbnails_columns' ) );

		// Show share icons
		add_action( 'woocommerce_single_product_summary', array( $this, 'share' ), 35 );

		// Change next and prev icon
		add_filter( 'woocommerce_pagination_args', array( $this, 'pagination_args' ) );

		// Show view detail after view cart button, will be displayed in quick view modal.
		add_action( 'woocommerce_after_add_to_cart_button', array( $this, 'view_detail_button' ) );

		// Change product rating html
		add_filter( 'woocommerce_product_get_rating_html', array( $this, 'product_rating_html' ), 10, 2 );

		// Change product stock html
		add_filter( 'woocommerce_stock_html', array( $this, 'product_stock_html' ), 10, 3 );

		// Change add to cart link
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );

		// Add products upsell display
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
		add_action( 'woocommerce_after_single_product_summary', array( $this, 'upsell_products' ), 15 );

		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		add_action( 'woocommerce_after_single_product', array( $this, 'related_products' ) );

		remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
	}

	/**
	 * Ajaxify update cart viewer
	 *
	 * @since 1.0
	 *
	 * @param array $fragments
	 *
	 * @return array
	 */
	function add_to_cart_fragments( $fragments ) {
		global $woocommerce;

		if ( empty( $woocommerce ) ) {
			return $fragments;
		}

		ob_start();
		?>

		<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ) ?>" class="cart-contents" title="<?php esc_attr_e( 'View your shopping cart', 'claudio' ) ?>">
			<span class="cart-icon"></span>
			<span class="mini-cart-counter"><?php echo intval( $woocommerce->cart->cart_contents_count ) ?> <span><?php _e( 'items', 'claudio' ) ?></span></span>
			/ <?php echo $woocommerce->cart->get_cart_total(); ?>
		</a>

		<?php
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}

	/**
	 * Remove default woocommerce styles
	 *
	 * @since  1.0
	 *
	 * @param  array $styles
	 *
	 * @return array
	 */
	function wc_styles( $styles ) {
		// unset( $styles['woocommerce-general'] );
		unset( $styles['woocommerce-layout'] );
		unset( $styles['woocommerce-smallscreen'] );

		return $styles;
	}

	/**
	 * Display a tool bar on top of product archive
	 *
	 * @since 1.0
	 */
	function shop_toolbar() {
		?>

		<div class="shop-toolbar">
			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
					<?php woocommerce_result_count() ?>
				</div>

				<div class="col-xs-12 col-sm-5 text-right">
					<div class="row">
						<div class="sort-by col-xs-7">
							<?php woocommerce_catalog_ordering() ?>
						</div>
						<div class="shop-view col-sm-5 hidden-xs">
							<a href="#" class="grid-view <?php echo $this->shop_view == 'grid' ? 'current' : '' ?>" data-view="grid"><i class="fa fa-th-large"></i></a>
							<a href="#" class="list-view <?php echo $this->shop_view == 'list' ? 'current' : '' ?>" data-view="list"><i class="fa fa-th-list"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php
	}

	/**
	 * Change the shop columns
	 *
	 * @since  1.0.0
	 * @param  int $columns The default columns
	 * @return int
	 */
	function shop_columns( $columns ) {
		if ( is_woocommerce() ) {

			$columns = intval( claudio_theme_option( 'product_columns' ) );

			if( empty($columns) ) {
				if ( is_product() ) {
					$columns = 3;
				} elseif ( ( is_woocommerce() || is_cart() ) && 'full-content' == $this->layout ) {
					$columns = 4;
				} elseif ( ( is_woocommerce() || is_cart() ) && 'full-content' != $this->layout ) {
					$columns = 3;
				}
			}
		}

		return $columns;
	}

	/**
	 * Add Bootstrap's column classes for product
	 *
	 * @since 1.0
	 *
	 * @param array  $classes
	 * @param string $class
	 * @param string $post_id
	 *
	 * @return array
	 */
	function product_class( $classes, $class = '', $post_id = '' ) {
		if ( ! $post_id || get_post_type( $post_id ) !== 'product' || is_single( $post_id ) ) {
			return $classes;
		}
		global $woocommerce_loop;

		$classes[] = 'col-sm-6 col-xs-6';
		$classes[] = 'col-md-' . (12 / $woocommerce_loop['columns']);

		return $classes;
	}

	/**
	 * Wrap product content
	 * Open a div
	 *
	 * @since 1.0
	 */
	function open_product_inner() {
		echo '<div class="product-inner clearfix">';
	}

	/**
	 * Wrap product content
	 * Close a div
	 *
	 * @since 1.0
	 */
	function close_product_inner() {
		echo '</div>';
	}

	/**
	 * Open product info wrapper
	 */
	function open_product_info() {
		echo '<div class="product-info">';
	}

	/**
	 * Close product info
	 * Close a div
	 *
	 * @since 1.0
	 */
	function close_product_info() {
		echo '</div>';
	}

	/**
	 * Display badge for new product or featured product
	 *
	 * @since 1.0
	 */
	function product_ribbons() {
		global $product;

		// Change the default sale ribbon
		if ( $product->is_on_sale() ) {
			echo '<span class="onsale ribbon">' . __( 'Sale', 'claudio' ) . '</span>';
			return;
		}

		// Ribbon for featured product
		if ( $product->is_featured() ) {
			echo '<span class="featured ribbon">' . __( 'Hot', 'claudio' ) . '</span>';
			return;
		}

		// If the product was published within the newness time frame display the new badge
		if ( ( time() - ( 60 * 60 * 24 * $this->new_duration ) ) < strtotime( get_the_time( 'Y-m-d' ) ) ) {
			echo '<span class="newness ribbon">' . __( 'New', 'claudio' ) . '</span>';
			return;
		}
	}

	/**
	 * WooCommerce Loop Product Content Thumbs
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function product_content_thumbnail() {
		global $product;
		$attachment_ids = $product->get_gallery_image_ids();

		if( count( $attachment_ids ) == 0 ) {
			echo '<div class="product-content-thumbnails product-thumbnail-single">';
		} else {
			echo '<div class="product-content-thumbnails">';
		}

		echo '<a href ="' . get_the_permalink() . '">';
		echo woocommerce_get_product_thumbnail();
		if( count( $attachment_ids ) > 0 ) {
			echo wp_get_attachment_image( $attachment_ids[0], 'shop_catalog' );
		}
		echo '</a>';

		echo '<div class="footer-product">';
		echo '<div class="footer-product-button">';
		printf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="btn-add-to-cart button %s product_type_%s %s"><i class="fa fa-shopping-cart" data-original-title="%s" rel="tooltip"></i></a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( $product->get_id() ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
			esc_attr( $product->get_type() ),
			$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
			esc_html( $product->add_to_cart_text() )
		);
		if( shortcode_exists( 'yith_compare_button' ) ) {
			echo do_shortcode( '[yith_compare_button]' );
		}
		if( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
			echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
		}
		echo '<span data-href="' . $product->get_permalink() . '"  class="product-quick-view"><i class="fa fa-search"' . ' data-original-title="' . esc_attr__( 'Quick View', 'claudio' ) . '" rel="tooltip"></i></span>';

		echo '</div>';
		echo '</div>';

		echo '</div>';
	}

	/**
	 * WooCommerce Single Product Thumbs
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function product_single_thumbnail_html( $html, $attachment_id, $post_id, $image_class ) {
		$image        = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
		$image_title  = esc_attr( get_the_title( $attachment_id ) );
		$image_single = wp_get_attachment_image_src( $attachment_id, 'shop_single' );
		$image_full   = wp_get_attachment_image_src( $attachment_id, 'full' );

		if( $image_single && $image_full ) {
			return sprintf( '<a href="%s" rel="bb-prettyPhoto[product-gallery]" data-src="%s" title="%s">%s</a>',
				esc_url( $image_full[0] ),
				esc_url( $image_single[0] ),
				esc_attr( $image_title ),
				$image
			);
		}
	}

	/**
	 * WooCommerce Single Product
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function product_single_html() {
		global $product, $post;

		if ( has_post_thumbnail() ) {
			$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
			$image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	=> $image_title,
				'alt'	=> $image_title
				) );
			$attachment_count = count( $product->get_gallery_image_ids() );

			return sprintf( '<a href="%s" class="woocommerce-main-image">%s</a>', esc_url( $image_link ), $image );
		}
		else {
			return sprintf( '<img src="%s" alt="%s">', esc_url( wc_placeholder_img_src() ), esc_attr__( 'Placeholder', 'woocommerce' ) );
		}
	}

	/**
	 * Display description of sub-category in list view
	 *
	 * @param  object $category
	 */
	function show_cat_desc( $category ) {
		printf( '<div class="category-desc" itemprop="description">%s</div>', $category->description );
	}

	/**
	 * Display product category before the title
	 */
	function product_categories() {
		$terms = get_the_terms( get_the_ID(), 'product_cat' );

		if( $terms ) {
			printf( '<div class="product-cat"><a href="%s" class="title-cat">%s</a></div>',
				esc_url( get_term_link( $terms[0]->slug, 'product_cat' ) ),
				$terms[0]->name
			);
		}
	}

	/**
	 * Show the product title in the product loop. By default this is an H2.
	 */
	function product_title() {
		echo '<h3 class="woocommerce-loop-product__title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h3>';
	}

	/**
	 * Change related products args to display in correct grid
	 *
	 * @param  array $args
	 *
	 * @return array
	 */
	function related_products_args( $args ) {
		$number = 'full-content' == $this->layout ? 4 : 3;

		$args['posts_per_page'] = 3;
		$args['columns']        = 1;

		return $args;
	}

	/**
	 * Change number of columns when display cross sells products
	 *
	 * @param  int $cl
	 * @return int
	 */
	function cross_sells_columns( $columns ) {
		return 3;
	}

	/**
	 * Change product thumbnails columns
	 *
	 * @return int
	 */
	function product_thumbnails_columns() {
		return 4;
	}


	/**
	 * Change next and previous icon of pagination nav
	 *
	 * @since  1.0
	 */
	function pagination_args( $args ) {
		$args['prev_text'] = '<i class="fa fa-angle-left"></i>';
		$args['next_text'] = '<i class="fa fa-angle-right"></i>';

		return $args;
	}

	/**
	 * Display Addthis sharing
	 *
	 * @since 1.0
	 */
	function share() {
		if ( $addthis_id = claudio_theme_option( 'addthis_profile_id' ) ) {
			printf(
				'<div class="addthis_native_toolbox addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_button_tweet"></a>
					<a class="addthis_button_google_plusone"></a>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=%s"></script>
				</div>',
				esc_attr( $addthis_id )
			);
		}
	}

	/**
	 * Display view detail button
	 *
	 * @since 1.0
	 */
	function view_detail_button() {
		global $product;
		printf( '<a href="%s" class="view-detail-button">%s</a>',
			esc_url( $product->get_permalink() ),
			__( 'View Detail', 'claudio' )
		);
	}

	/**
	 * Display product rating
	 *
	 * @since 1.0
	 */
	function product_rating_html( $rating_html, $rating ) {
		$rating_html  = '<div class="star-rating" title="' . sprintf( esc_attr__( 'Rated %s out of 5', 'claudio' ), $rating ) . '">';
		$rating_html .= '<span style="width:' . ( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . __( 'out of 5', 'claudio' ) . '</span>';
		$rating_html .= '</div>';

		return $rating_html;
	}

	/**
	 * Display product stock
	 *
	 * @since 1.0
	 */
	function product_stock_html( $availability_html, $availability, $product ) {
		$availability      = $product->get_availability();
		$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . __( 'Availability: ', 'claudio' )  . '<span>' . esc_html( $availability['availability'] ) . '</span>' . '</p>';

		return $availability_html;
	}

	/**
	 * Display upsell products
	 *
	 * @since 1.0
	 */
	function upsell_products() {
		woocommerce_upsell_display( 3, 3 );
	}

	/**
	 * Display related products
	 *
	 * @since 1.0
	 */
	function related_products() {
		?>
		<div class="related-products col-xs-12 col-sm-12 col-md-3">
			<?php woocommerce_output_related_products(); ?>
		</div>
		<?php
	}
}
