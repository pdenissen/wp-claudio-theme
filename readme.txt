Claudio – Responsive WooCommerce WordPress Themes
------------------------------

Claudio is a Responsive WooCommerce WordPress Theme. The theme is WooCommerce Ready and will help you build any kind of online store. This theme is suited for any type of website, e-commerce or personal or business use.


Change log:

Version 1.2.0
- Update WooCommerce's templates

Version 1.1.7
- Fix: Child theme doesn't load styles. Please update both parent theme and child theme to fix this.

Version 1.1.6
- Fix: Wrong style on page Theme Options

Version 1.1.5:
- Fix: Cart page doesn't run ajax
- Fix: styles in some pages
- Update: plugin Revolution Slider 5.2.6

Version 1.1.4:
- Fix: compatible with WooCommerce 2.6.x

Version 1.1.3:
- Update: plugin Visual Composer
- Update: plugin Revolution Slider
- Fix: Instagram shortcode doesn't work. Need to re-config to make it work.

Version 1.1.2:
- Update: plugin Visual Composer
- Update: plugin Revolution Slider

Version 1.1.1:
- Add: An option(in the theme option) to allow change product columns on shop page.
- Fix: Missing style shipping in cart, checkout page

Version 1.1:
- Update: Compatible with WooCommerce 2.5.0.
- Update: Update Visual Composer plugin 4.9.2
- Update: Update Slider Revolution plugin
- Fix: Missing style shop list view on the mobile

Version 1.0.8:
- Update: Update Visual Composer plugin

Version 1.0.7:
- Update: Update Visual Composer plugin
- Update: Update Slider Revolution plugin
- Fix: links of images carousel get a /br/

Version 1.0.6:
- Update: Update Visual Composer plugin
- Fix: The child theme is missing style

Version 1.0.5:
- Update: Support WPML
- Fix: Comments style problem
- Fix: Problem about style when product's name is too long

Version 1.0.4:
- Update: Update Visual Composer plugin

Version 1.0.3
- Update: Update Slider Revolution plugin
- Update: Update Visual Composer plugin
- Fix: breadcrumbs won't show on single product

Version 1.0.2
- Update: Update Slider Revolution plugin
- Fix: Bug on some hostings which have old version of PHP installed

Version 1.0.1
- Fix: Bug on some hostings which have old version of PHP installed

Version 1.0
- Initial
