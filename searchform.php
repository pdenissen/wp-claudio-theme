<form class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<i class="fa fa-search"></i>
	<input type="text" placeholder="<?php _e( 'Search entire store here&hellip;', 'claudio' ); ?>" name="s" class="search-field">
</form>
