<?php
/**
 * Claudio_Mini_Cart_Widget widget class
 *
 * @since 1.0
 */
class Claudio_Mini_Cart_Widget extends WP_Widget {
	/**
	 * Holds widget default settings, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Class constructor
	 * Set up the widget
	 *
	 * @return Claudio_Mini_Cart_Widget
	 */
	function __construct() {
		$this->defaults = array(
			'title' => ''
		);

		parent::__construct(
			'ta-mini-cart-widget',
			__( 'Claudio - Mini Cart', 'claudio' ),
			array(
				'classname'   => 'ta-mini-cart-widget',
				'description' => __( 'Display minimal shoping cart', 'claudio' ),
			)
		);
	}

	/**
	 * Outputs the HTML for this widget.
	 *
	 * @param array $args     An array of standard parameters for widgets in this theme
	 * @param array $instance An array of settings for this widget instance
	 *
	 * @return void Echoes it's output
	 */
	public function widget( $args, $instance ) {
		if ( ! function_exists( 'is_woocommerce' ) ) {
			return;
		}

		global $woocommerce;
		$instance = wp_parse_args( $instance, $this->defaults );
		extract( $args );

		if ( $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) ) {
			echo $before_title . esc_html( $title ) . $after_title;
		}

		echo $before_widget;
		?>

		<div class="mini-cart woocommerce">
			<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ) ?>" class="cart-contents" title="<?php esc_attr_e( 'View your shopping cart', 'claudio' ) ?>">
				<span class="cart-icon"></span>
				<span class="mini-cart-counter"><?php echo intval( $woocommerce->cart->cart_contents_count ) ?> <span><?php _e( 'items', 'claudio' ) ?></span></span>
				/ <?php echo $woocommerce->cart->get_cart_total(); ?>
			</a>

			<div class="widget_shopping_cart_content">
				<?php woocommerce_mini_cart(); ?>
			</div>
		</div>

		<?php
		echo $after_widget;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title', 'claudio' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>">
		</p>

		<?php
	}
}
