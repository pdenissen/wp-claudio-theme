<?php
/**
 * Hooks for displaying elements on footer
 *
 * @package Claudio
 */

/**
 * Display footer widgets
 *
 * @since 1.0.0
 */
function claudio_footer_widgets() {
	if ( ! claudio_theme_option( 'footer_widgets' ) ) {
		return;
	}
	?>
	<div id="footer-widgets" class="footer-widgets widgets-area">
		<div class="container">
			<div class="row">
				<?php
				$columns = max( 1, absint( claudio_theme_option( 'footer_widget_columns' ) ) );

				if ( 5 == $columns ) {
					$col_class = 'col-xs-12 col-sm-5ths';
				} else {
					$col_class = 'col-xs-12 col-sm-6 col-md-' . floor( 12 / $columns );
				}
				for ( $i = 1; $i <= $columns; $i++ ) :
				?>
					<div class="footer-sidebar footer-<?php echo esc_attr( $i ) ?> <?php echo esc_attr( $col_class ) ?>">
						<?php dynamic_sidebar( "footer-$i" ); ?>
					</div>
				<?php endfor; ?>
			</div>
		</div>
	</div>
	<?php
}
add_action( 'claudio_before_footer', 'claudio_footer_widgets', 5 );

/**
 * Display extra content right after footer widgets
 *
 * @since 1.0.0
 */
function claudio_footer_extra() {
	$extra = trim( wp_kses( claudio_theme_option( 'footer_extra' ), wp_kses_allowed_html( 'post' ) ) );
	if ( ! $extra ) {
		return;
	}
	?>
	<div id="footer-extra" class="footer-extra">
		<div class="container">
			<?php echo do_shortcode( $extra ) ?>
		</div>
	</div>
	<?php
}
add_action( 'claudio_before_footer', 'claudio_footer_extra', 10 );

/**
 * Display footer nav menu before site footer
 *
 * @since 1.0.0
 */
function claudio_footer_menu() {
	if ( ! has_nav_menu( 'footer' ) ) {
		return;
	}
	?>
	<nav id="footer-nav" class="footer-nav nav">
		<div class="container">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'footer',
				'container'      => false,
			) );
			?>
		</div>
	</nav>
	<?php
}
add_action( 'claudio_before_footer', 'claudio_footer_menu', 15 );

/**
 * Display a layer to close canvas panel everywhere inside page
 *
 * @since 1.0.0
 */
function claudio_site_canvas_layer() {
	?>
	<div id="off-canvas-layer" class="off-canvas-layer"></div>
	<?php
}
add_action( 'claudio_after_footer', 'claudio_site_canvas_layer' );

/**
 * Add off canvas menu to footer
 *
 * @since 1.0.0
 */
function claudio_off_canvas_menu() {
	if ( 'logo-center' != claudio_theme_option( 'header_layout' ) ) {
		return;
	}

	?>
	<nav id="nav-panel" class="main-nav off-canvas-panel" role="navigation">
		<?php
		wp_nav_menu( array(
			'theme_location' => 'primary',
			'container'      => false,
		) );
		?>
	</nav>
	<?php
}
add_action( 'wp_footer', 'claudio_off_canvas_menu' );

/**
 * Dispaly off canvas menu for small screen devices
 *
 * @since 1.0.0
 */
function claudio_mobile_menu() {
	$location = has_nav_menu( 'mobile' ) ? 'mobile' : 'primary';

	?>
	<nav id="mobile-nav-panel" class="main-nav mobile-nav off-canvas-panel hidden-md hidden-lg" role="navigation">
		<?php
		wp_nav_menu( array(
			'theme_location' => $location,
			'container'      => false,
		) );
		?>
	</nav>
	<?php
}
add_action( 'wp_footer', 'claudio_mobile_menu' );

/**
 * Add off canvas shopping cart to footer
 *
 * @since 1.0.0
 */
function claudio_off_canvas_cart() {
	if ( ! function_exists( 'woocommerce_mini_cart' ) ) {
		return;
	}
	?>
	<div id="cart-panel" class="cart-panel woocommerce off-canvas-panel">
		<div class="widget_shopping_cart_content">
			<?php woocommerce_mini_cart(); ?>
		</div>
	</div>
	<?php
}
add_action( 'wp_footer', 'claudio_off_canvas_cart' );

/**
 * Add a modal on the footer, for displaying product quick view
 *
 * @since 1.0.0
 */
function claudio_quick_view_modal() {
	?>
	<div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" data-id="<?php echo esc_attr( claudio_theme_option( 'addthis_profile_id' ) ); ?>">
		<div class="item-detail">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i><span class="sr-only"><?php _e( 'Close', 'claudio' ) ?></span></button>
					</div>
					<div class="woocommerce">
						<div class="modal-body product"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
add_action( 'wp_footer', 'claudio_quick_view_modal' );

/**
 * Add scripts to footer
 *
 * @since 1.0.0
 */
function claudio_footer_scripts() {
	$scripts = '';

	// Get custom js from theme options
	$scripts .= claudio_theme_option( 'footer_scripts' );

	// Get custom js from singular page
	if ( is_singular() && $custom_js = claudio_get_meta( 'custom_js' ) ) {
		$scripts .= $custom_js;
	}

	echo $scripts;
}
add_action( 'wp_footer', 'claudio_footer_scripts' );
