<?php
/**
 * Hooks for template header
 *
 * @package Claudio
 */


if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function claudio_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}
		global $page, $paged;
		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}
		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( __( 'Page %s', 'claudio' ), max( $paged, $page ) );
		}
		return $title;
	}
	add_filter( 'wp_title', 'claudio_wp_title', 10, 2 );
	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function claudio_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'claudio_render_title' );
endif;

/**
 * Enqueue scripts and styles.
 *
 * @since 1.0
 */
function claudio_enqueue_scripts() {
	/* Register and enqueue styles */
	wp_register_style( 'font-awesome', THEME_URL . '/css/font-awesome.min.css', array(), '4.3.0' );
	wp_register_style( 'bootstrap', THEME_URL . '/css/bootstrap.min.css', array(), '3.3.2' );
	wp_register_style( 'claudio-fonts', claudio_fonts_url(), array(), THEME_VERSION );

	if( ! wp_style_is( 'js_composer_front', 'enqueued' ) && wp_style_is( 'js_composer_front', 'registered') ) {
		wp_enqueue_style('js_composer_front');
	}

	wp_enqueue_style( 'claudio', get_template_directory_uri() . '/style.css', array( 'claudio-fonts', 'bootstrap', 'font-awesome' ), THEME_VERSION );

	// Load custom color scheme file
	if ( intval( claudio_theme_option( 'custom_color_scheme' ) ) && claudio_theme_option( 'custom_color_1' ) ) {
		$upload_dir = wp_upload_dir();
		$dir        = path_join( $upload_dir['baseurl'], 'custom-css' );
		$file       = $dir . '/color-scheme.css';
		wp_enqueue_style( 'claudio-color-scheme', $file, THEME_VERSION );
	}

	/** Register and enqueue scripts */
	$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_register_script( 'claudio-plugins', THEME_URL . "/js/plugins$min.js", array( 'jquery' ), THEME_VERSION, true );
	wp_enqueue_script( 'claudio', THEME_URL . "/js/scripts$min.js", array( 'claudio-plugins' ), THEME_VERSION, true );

	$script_name = 'wc-add-to-cart-variation';
	if( wp_script_is( $script_name, 'registered' ) && ! wp_script_is( $script_name, 'enqueued' ) ) {
		wp_enqueue_script( $script_name );
	}

	wp_localize_script( 'claudio', 'claudio', array(
		'ajax_url'  => admin_url( 'admin-ajax.php' ),
		'nonce'     => wp_create_nonce( '_claudio_nonce' ),
		'direction' => is_rtl() ? 'rtl' : '',
	) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'claudio_enqueue_scripts' );

/**
 * Get favicon and home screen icons
 *
 * @since  1.0
 */
function claudio_site_icons() {
	if ( function_exists( 'wp_site_icon' ) ) {
		return;
	}

	$favicon            = claudio_theme_option( 'favicon' );
	$header_icons       =  ( $favicon ) ? '<link rel="icon" type="image/x-ico" href="' . esc_url( $favicon ) . '" />' : '';

	$icon_ipad_retina   = claudio_theme_option( 'icon_ipad_retina' );
	$header_icons       .= ( $icon_ipad_retina ) ? '<link rel="apple-touch-icon" href="' . esc_url( $icon_ipad_retina ) . '" />' : '';

	$icon_ipad          = claudio_theme_option( 'icon_ipad' );
	$header_icons       .= ( $icon_ipad ) ? '<link rel="apple-touch-icon" href="' . esc_url( $icon_ipad ) . '" />' : '';

	$icon_iphone_retina = claudio_theme_option( 'icon_iphone_retina' );
	$header_icons       .= ( $icon_iphone_retina ) ? '<link rel="apple-touch-icon" href="' . esc_url( $icon_iphone_retina ). '" />' : '';

	$icon_iphone        = claudio_theme_option( 'icon_iphone' );
	$header_icons       .= ( $icon_iphone ) ? '<link rel="apple-touch-icon" href="' . esc_url( $icon_iphone ) . '" />' : '';

	echo $header_icons;
}
add_action( 'wp_head', 'claudio_site_icons' );

/**
 * Custom scripts and styles on header
 *
 * @since  1.0.0
 */
function claudio_header_scripts() {
	/**
	 * All Custom CSS rules
	 */
	$inline_css = '';

	// Background
	$background = claudio_theme_option( 'background' );
	if ( $background && 'boxed' == claudio_theme_option( 'layout_style' ) ) {
		$bg_css = ! empty( $background['color'] ) ? "background-color: {$background['color']};" : '';

		if ( ! empty( $background['image'] ) ) {
			$bg_css .= "background-image: url({$background['image']});";
			$bg_css .= "background-repeat: {$background['repeat']};";
			$bg_css .= "background-position: {$background['position_x']} {$background['position_y']};";
			$bg_css .= "background-attachment: {$background['attachment']};";
			$bg_css .= ! empty( $background['size'] ) ? "background-size: {$background['size']};" : '';
		}

		if ( $bg_css ) {
			$inline_css .= 'body.boxed {' . $bg_css . '}';
		}
	}

	// Logo
	$logo_size_width = intval( claudio_theme_option( 'logo_size_width' ) );
	$logo_css = $logo_size_width ? 'width:' . $logo_size_width . 'px; ' : '';

	$logo_size_height = intval( claudio_theme_option( 'logo_size_height' ) );
	$logo_css .= $logo_size_height ? 'height:' . $logo_size_height . 'px; ' : '';

	$logo_margin_top = intval( claudio_theme_option( 'logo_margin_top' ) );
	$logo_css .= $logo_margin_top ? 'margin-top:' . $logo_margin_top . 'px;' : '';

	$logo_margin_right = intval( claudio_theme_option( 'logo_margin_right' ) );
	$logo_css .= $logo_margin_right ? 'margin-right:' . $logo_margin_right . 'px;' : '';

	$logo_margin_bottom = intval( claudio_theme_option( 'logo_margin_bottom' ) );
	$logo_css .= $logo_margin_bottom ? 'margin-bottom:' . $logo_margin_bottom . 'px;' : '';

	$logo_margin_left = intval( claudio_theme_option( 'logo_margin_left' ) );
	$logo_css .= $logo_margin_left ? 'margin-left:' . $logo_margin_bottom . 'px;' : '';

	if ( ! empty( $logo_css ) ) {
		$inline_css .= '.site-header .site-branding .logo img ' . ' {' . $logo_css . '}';
	}

	// Banner
	if ( claudio_theme_option( 'shop_banner' ) && function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
		$banner = claudio_theme_option( 'banner_image' );
		$inline_css .= ".site-banner { background-image: url($banner); }";
	}

	// Custom CSS from singule post/page
	$css_custom = claudio_get_meta( 'custom_page_css' ) . claudio_theme_option( 'custom_css' );
	if ( ! empty( $css_custom ) ) {
		$inline_css .= $css_custom;
	}

	// Output CSS
	if ( ! empty( $inline_css ) ) {
		echo '<style type="text/css">' . $inline_css . '</style>';
	}

	/**
	 * Custom header javascripts
	 */
	$custom_js = '';
	if ( $header_scripts = claudio_theme_option( 'header_scripts' ) ) {
		$custom_js .= $header_scripts;
	}

	// Output javascipt
	if( ! empty( $custom_js ) ) {
		echo $custom_js;
	}
}
add_action( 'wp_head', 'claudio_header_scripts' );

/**
 * Display topbar on top of site
 *
 * @since 1.0.0
 */
function claudio_show_topbar() {
	if ( ! claudio_theme_option( 'topbar' ) ) {
		return;
	}
	?>
	<div id="topbar" class="topbar hidden-xs">
		<div class="container">
			<div class="row">
				<div class="topbar-left topbar-widgets col-xs-12 col-sm-6">
					<?php dynamic_sidebar( 'topbar-left' ); ?>
				</div>

				<div class="topbar-right topbar-widgets col-xs-12 col-sm-6">
					<?php dynamic_sidebar( 'topbar-right' ); ?>
				</div>
			</div>
		</div>
	</div>
	<?php
}
add_action( 'claudio_before_header', 'claudio_show_topbar', 5 );

/**
 * Display the site header
 *
 * @since 1.0.0
 */
function claudio_show_header() {
	switch( claudio_theme_option( 'header_layout' ) ) {
		case 'logo-center':
			?>
			<div class="header-main">
				<div class="container">
					<div class="row">
						<div class="menu-toggle col-xs-2 col-sm-2 col-md-3">
							<a href="#" class="toggle-nav" id="toggle-nav"><span class="bars-icon"></span></a>
						</div>
						<div class="site-branding text-center col-xs-8 col-sm-8 col-md-6">
							<?php get_template_part( 'parts/logo' ); ?>
						</div>
						<div class="cart-toggle col-xs-2 col-sm-2 col-md-3 text-right">
							<div class="mini-cart">
								<a href="#" class="toggle-cart" id="toggle-cart">
									<span class="cart-icon"></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
			break;

		case 'logo-side':
			?>
			<div class="container">
				<div class="row">
					<div class="menu-toggle col-xs-2 text-left hidden-md hidden-lg">
						<a href="#" class="toggle-nav" id="toggle-nav"><span class="bars-icon"></span></a>
					</div>
					<div class="site-branding col-xs-8 col-sm-8 col-md-3">
						<?php get_template_part( 'parts/logo' ); ?>
					</div>
					<div class="cart-toggle col-xs-2 text-right hidden-md hidden-lg">
						<div class="mini-cart">
							<a href="#" class="toggle-cart" id="toggle-cart">
								<span class="cart-icon"></span>
							</a>
						</div>
					</div>

					<nav id="site-navigation" class="main-nav primary-nav nav col-xs-12 col-md-9 hidden-xs hidden-sm" role="navigation">
						<?php
						if ( has_nav_menu( 'primary' ) ) {
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'container' => false,
								'walker' => new Claudio_Walker_Mega_Menu,
							) );
						}
						?>
					</nav>
				</div>
			</div>
			<?php
			break;

		default:
			?>
			<div class="site-branding text-center">
				<div class="container">
					<div class="row">
						<div class="menu-toggle col-xs-2 text-left hidden-md hidden-lg">
							<a href="#" class="toggle-nav" id="toggle-nav"><span class="bars-icon"></span></a>
						</div>
						<div class="col-xs-8 col-sm-8 col-md-12">
							<?php get_template_part( 'parts/logo' ); ?>
						</div>
						<div class="cart-toggle col-xs-2 text-right hidden-md hidden-lg">
							<div class="mini-cart">
								<a href="#" class="toggle-cart" id="toggle-cart">
									<span class="cart-icon"></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<nav id="site-navigation" class="main-nav primary-nav nav hidden-xs hidden-sm" role="navigation">
				<div class="container">
					<?php
					if ( has_nav_menu( 'primary' ) ) {
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'container' => false,
							'walker' => new Claudio_Walker_Mega_Menu,
						) );
					}
					?>
				</div>
			</nav>

			<?php
			break;
	}
}
add_action( 'claudio_header', 'claudio_show_header' );

/**
 * Show a banner on the shop page
 *
 * @since 1.0.0
 */
function claudio_show_banner() {
	if ( ! claudio_theme_option( 'shop_banner' ) ) {
		return;
	}

	if ( ! function_exists( 'is_woocommerce' ) ) {
		return;
	}

	if ( ! is_woocommerce() || is_singular() ) {
		return;
	}

	$image = claudio_theme_option( 'banner_image' );
	$text  = do_shortcode( wp_kses( claudio_theme_option( 'banner_text' ), wp_kses_allowed_html( 'post' ) ) );
	if ( empty( $image ) && empty( $text ) ) {
		return;
	}
	$text = wp_kses( $text, wp_kses_allowed_html( 'post' ) );
	?>
	<div class="site-banner title-area text-center">
		<div class="container">
			<?php echo $text ?>
		</div>
	</div>
	<?php
}
add_action( 'claudio_after_header', 'claudio_show_banner' );

/**
 * Display a breadcrumb bellow header and banner
 *
 * @since 1.0.0
 */
function claudio_show_breadcrumb() {
	if ( ! claudio_theme_option( 'breadcrumb' ) ) {
		return;
	}

	if ( is_singular() && claudio_get_meta( 'hide_breadcrumb' ) ) {
		return;
	}

	$pages = claudio_theme_option( 'show_breadcrumb' );
	if ( empty( $pages ) ) {
		return;
	}

	if ( is_front_page() ) {
		return;
	} elseif ( is_home() && ! is_front_page() && ! in_array( 'blog', $pages ) ) {
		return;
	} elseif ( function_exists( 'is_shop' ) && is_shop() && ! in_array( 'shop', $pages ) ) {
		return;
	} elseif ( function_exists( 'is_product' ) && is_product() && ! in_array( 'product', $pages ) ) {
		return;
	} elseif ( is_singular( 'post' ) && ! in_array( 'post', $pages ) ) {
		return;
	} elseif ( is_page() && ! in_array( 'page', $pages ) ) {
		return;
	}
	?>
	<nav class="breadcrumbs">
		<div class="container">
			<?php
			claudio_breadcrumbs( array(
				'separator' => '/',
				'before'    => '',
				'taxonomy'  => function_exists( 'is_woocommerce' ) && is_woocommerce() ? 'product_cat' : 'category',
			) );
			?>
		</div>
	</nav>
	<?php
}
add_action( 'claudio_after_header', 'claudio_show_breadcrumb' );

/**
 * Change archive label for shop page
 *
 * @since  1.0.0
 *
 * @param  array $args
 *
 * @return array
 */
function claudio_breadcrumbs_labels( $args ) {
	if ( function_exists( 'is_shop' ) && is_shop() ) {
		$args['labels']['archive'] = __( 'Shop', 'claudio' );
	}

	return $args;
}
add_filter( 'claudio_breadcrumbs_args', 'claudio_breadcrumbs_labels' );
