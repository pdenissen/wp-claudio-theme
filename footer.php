<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Claudio
 */
?>
		<?php if ( ! is_page_template( 'template-full-width.php' ) ) : ?>
				</div> <!-- .row -->
			</div><!-- .container -->
		<?php endif; ?>
	</div><!-- #content -->

	<?php do_action( 'claudio_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="site-info col-xs-12 col-sm-12 col-md-6">
					<?php echo do_shortcode( wp_kses( claudio_theme_option( 'footer_copyright' ), wp_kses_allowed_html( 'post' ) ) ); ?>
				</div><!-- .site-info -->
				<div class="socials text-right col-xs-12 col-sm-12 col-md-6">
					<?php
					$socials = array_filter( (array) claudio_theme_option( 'socials' ) );
					foreach( $socials as $social => $url ) {
						if ( $social == 'google' ) {
							$social = 'google-plus';
						} elseif ( $social == 'vimeo' ) {
							$social = 'vimeo-square';
						} elseif ( $social == 'mail' ) {
							$social = 'envelope';
						}
						printf( '<a href="%s" target="_blank"><i class="fa fa-%s"></i></a>', esc_url( $url ), esc_attr( $social ) );
					}
					?>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->

	<?php do_action( 'claudio_after_footer' ); ?>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
