<?php
/**
 * Register default theme options fields
 *
 * @package Claudio
 */


/**
 * Register theme options fields
 *
 * @since  1.0
 *
 * @return array Theme options fields
 */
function claudio_theme_option_fields() {
    $options = array();

    // Help information
    $options['help'] = array(
        'document' => 'http://themealien.com/docs/claudio/',
        'support'  => 'http://themealien.com/support/claudio/',
    );


    // Sections
    $options['sections'] = array(
        'general'    => array(
            'icon'  => 'cog',
            'title' => __('General', 'claudio'),
        ),
        'layout'     => array(
            'icon'  => 'grid',
            'title' => __('Layout', 'claudio'),
        ),
        'style'      => array(
            'icon'  => 'palette',
            'title' => __('Style', 'claudio'),
        ),
        'header'     => array(
            'icon'  => 'browser',
            'title' => __('Header', 'claudio'),
        ),
        'breadcrumb' => array(
            'icon'  => 'flow-tree',
            'title' => __('Breadcrumb', 'claudio'),
        ),
        'content'    => array(
            'icon'  => 'news',
            'title' => __('Content', 'claudio'),
        ),
        'shop'       => array(
            'icon'  => 'shopping-cart',
            'title' => __('Shop', 'claudio'),
        ),
        'footer'     => array(
            'icon'  => 'rss',
            'title' => __('Footer', 'claudio'),
        ),
        'export'     => array(
            'icon'  => 'upload-to-cloud',
            'title' => __('Backup - Restore', 'claudio'),
        ),
    );

    // Fields
    $options['fields']            = array();
    $options['fields']['general'] = array();
    if ( version_compare( $GLOBALS['wp_version'], '4.3', '<' ) ) :
        $options['fields']['general'] = array(
            array(
                'name'  => 'favicon',
                'label' => __('Favicon', 'claudio'),
                'type'  => 'icon',
            ),
            array(
                'name'     => 'home_screen_icons',
                'label'    => __('Home Screen Icons', 'claudio'),
                'desc'     => __('Select image file that will be displayed on home screen of handheld devices.', 'claudio'),
                'type'     => 'group',
                'children' => array(
                    array(
                        'name'    => 'icon_ipad_retina',
                        'type'    => 'icon',
                        'subdesc' => __('IPad Retina (144x144px)', 'claudio'),
                    ),
                    array(
                        'name'    => 'icon_ipad',
                        'type'    => 'icon',
                        'subdesc' => __('IPad (72x72px)', 'claudio'),
                    ),

                    array(
                        'name'    => 'icon_iphone_retina',
                        'type'    => 'icon',
                        'subdesc' => __('IPhone Retina (114x114px)', 'claudio'),
                    ),

                    array(
                        'name'    => 'icon_iphone',
                        'type'    => 'icon',
                        'subdesc' => __('IPhone (57x57px)', 'claudio'),
                    )
                )
            ),
            array(
                'type' => 'divider',
            ),
        );
    endif;

    $options['fields']['general'] = array_merge(
        $options['fields']['general'], array(
            array(
                'name'    => 'addthis_profile_id',
                'label'   => __('AddThis Profile ID', 'claudio'),
                'desc'    => __('This ID is used for product sharing function', 'claudio'),
                'subdesc' => __('Please go <a href="https://www.addthis.com/settings/publisher" target="_blank">here</a> to get your Addthis Profile ID. You must have an account on this site.', 'claudio'),
                'type'    => 'text',
                'size'    => 'large',
            ),
        )
    );


    $options['fields']['layout'] = array(
        array(
            'name'    => 'layout_style',
            'label'   => __('Layout Style', 'claudio'),
            'type'    => 'image_toggle',
            'default' => 'wide',
            'options' => array(
                'wide'  => CLAUDIO_OPTIONS_URL . 'img/layout/wide.png',
                'boxed' => CLAUDIO_OPTIONS_URL . 'img/layout/box.png',
            )
        ),
        array(
            'type' => 'divider'
        ),
        array(
            'name'    => 'default_layout',
            'label'   => __('Default Layout', 'claudio'),
            'desc'    => __('Default layout for whole site', 'claudio'),
            'type'    => 'image_toggle',
            'default' => 'content-sidebar',
            'options' => array(
                'full-content'    => CLAUDIO_OPTIONS_URL . 'img/sidebars/empty.png',
                'sidebar-content' => CLAUDIO_OPTIONS_URL . 'img/sidebars/single-left.png',
                'content-sidebar' => CLAUDIO_OPTIONS_URL . 'img/sidebars/single-right.png',
            )
        ),
        array(
            'name'    => 'page_layout',
            'label'   => __('Page Layout', 'claudio'),
            'desc'    => __('Default layout for pages', 'claudio'),
            'type'    => 'image_toggle',
            'default' => 'full-content',
            'options' => array(
                'full-content'    => CLAUDIO_OPTIONS_URL . 'img/sidebars/empty.png',
                'sidebar-content' => CLAUDIO_OPTIONS_URL . 'img/sidebars/single-left.png',
                'content-sidebar' => CLAUDIO_OPTIONS_URL . 'img/sidebars/single-right.png',
            )
        ),
        array(
            'name'    => 'shop_layout',
            'label'   => __('Shop Layout', 'claudio'),
            'desc'    => __('Default layout for shop, product archive pages', 'claudio'),
            'type'    => 'image_toggle',
            'default' => 'sidebar-content',
            'options' => array(
                'full-content'    => CLAUDIO_OPTIONS_URL . 'img/sidebars/empty.png',
                'sidebar-content' => CLAUDIO_OPTIONS_URL . 'img/sidebars/single-left.png',
                'content-sidebar' => CLAUDIO_OPTIONS_URL . 'img/sidebars/single-right.png',
            )
        ),
    );

    $options['fields']['style'] = array(
        array(
            'type'     => 'background',
            'name'     => 'background',
            'label'    => __('Background', 'claudio'),
            'desc'     => __('Set background for website. It only works when your layout style is boxed.', 'claudio'),
            'patterns' => array(
                THEME_URL . '/img/patterns/p1.png',
                THEME_URL . '/img/patterns/p2.png',
                THEME_URL . '/img/patterns/p3.png',
                THEME_URL . '/img/patterns/p4.png',
                THEME_URL . '/img/patterns/p5.png',
            )
        ),
        array(
            'type' => 'divider',
        ),
        array(
            'name'    => 'color_scheme',
            'label'   => __('Color Scheme', 'claudio'),
            'desc'    => __('Select color scheme for website', 'claudio'),
            'type'    => 'color_scheme',
            'default' => '',
            'options' => array(
                ''             => '#dc4f4f',
                'blue'         => '#00ADEF',
                'turquoise'    => '#2DCC70',
                'pink'         => '#FF0066',
                'teal'         => '#1693A5',
                'gold'         => '#FBB829',
                'downy'        => '#6dcda7',
                'atlantis'     => '#8cc732',
                'red'          => '#FF0000',
                'violet'       => '#D31996',
                'jgger'        => '#420943',
                'blaze-orange' => '#FF6600',
            )
        ),
        array(
            'name'     => 'custom_color_scheme',
            'label'    => __('Custom Color Scheme', 'claudio'),
            'desc'     => __('Enable custom color scheme to pick your own color scheme', 'claudio'),
            'type'     => 'group',
            'layout'   => 'vertical',
            'children' => array(
                array(
                    'name'    => 'custom_color_scheme',
                    'type'    => 'switcher',
                    'default' => false,
                ),
                array(
                    'name'    => 'custom_color_1',
                    'type'    => 'color',
                    'subdesc' => __('Custom Color', 'claudio'),
                ),
            )
        ),
        array(
            'type' => 'divider',
        ),
        array(
            'name'     => 'custom_css',
            'label'    => __('Custom CSS', 'claudio'),
            'type'     => 'code_editor',
            'language' => 'css',
            'subdesc'  => __('Enter your custom style rules here', 'claudio')
        ),
    );

    $options['fields']['header'] = array(
        array(
            'name'    => 'topbar',
            'label'   => __('Topbar', 'claudio'),
            'desc'    => __('Enable the topbar on the top of site', 'claudio'),
            'subdesc' => sprintf(__('Please go to <a href="%s">Widgets</a> to drag widgets to topbar', 'claudio'), admin_url('widgets.php')),
            'type'    => 'switcher',
        ),
        array(
            'type' => 'divider',
        ),
        array(
            'name'    => 'header_layout',
            'label'   => __('Header Layout', 'claudio'),
            'desc'    => __('Select default layout for site header', 'claudio'),
            'type'    => 'image_toggle',
            'default' => 'logo-above',
            'options' => array(
                'logo-above'  => THEME_URL . '/img/headers/header-1.png',
                'logo-center' => THEME_URL . '/img/headers/header-2.png',
                'logo-side'   => THEME_URL . '/img/headers/header-3.png',
            )
        ),
        array(
            'name'    => 'header_color',
            'label'   => __('Header Color', 'claudio'),
            'desc'    => __('Select color scheme for site header', 'claudio'),
            'type'    => 'toggle',
            'default' => 'light',
            'options' => array(
                'light' => __('Light', 'claudio'),
                'dark'  => __('Dark', 'claudio'),
            ),
        ),
        array(
            'type' => 'divider',
        ),
        array(
            'name'  => 'logo',
            'label' => __('Logo', 'claudio'),
            'desc'  => __('Select logo from media library or upload a new one', 'claudio'),
            'type'  => 'image',
        ),
        array(
            'name'     => 'logo_size',
            'label'    => __('Logo Size (Optional)', 'claudio'),
            'desc'     => __('If the Retina Logo uploaded, please enter the size of the Standard Logo just upload above (not the Retina Logo)', 'claudio'),
            'type'     => 'group',
            'children' => array(
                array(
                    'name'    => 'logo_size_width',
                    'type'    => 'number',
                    'subdesc' => __('(Width)', 'claudio'),
                    'suffix'  => 'px'
                ),
                array(
                    'name'    => 'logo_size_height',
                    'type'    => 'number',
                    'subdesc' => __('(Height)', 'claudio'),
                    'suffix'  => 'px'
                ),
            )
        ),
        array(
            'name'     => 'logo_margin',
            'label'    => __('Logo Margin', 'claudio'),
            'type'     => 'group',
            'children' => array(
                array(
                    'name'    => 'logo_margin_top',
                    'type'    => 'number',
                    'size'    => 'mini',
                    'subdesc' => __('top', 'claudio'),
                    'suffix'  => 'px'
                ),
                array(
                    'name'    => 'logo_margin_right',
                    'type'    => 'number',
                    'size'    => 'mini',
                    'subdesc' => __('right', 'claudio'),
                    'suffix'  => 'px'
                ),
                array(
                    'name'    => 'logo_margin_bottom',
                    'type'    => 'number',
                    'size'    => 'mini',
                    'subdesc' => __('bottom', 'claudio'),
                    'suffix'  => 'px'
                ),
                array(
                    'name'    => 'logo_margin_left',
                    'type'    => 'number',
                    'size'    => 'mini',
                    'subdesc' => __('left', 'claudio'),
                    'suffix'  => 'px'
                ),
            )
        ),
        array(
            'type' => 'divider',
        ),
        array(
            'name'     => 'menu_extra',
            'label'    => __('Menu Extra', 'claudio'),
            'desc'     => __('Display extra items at the end of primary menu', 'claudio'),
            'type'     => 'checkbox_list',
            'multiple' => true,
            'options'  => array(
                'cart'   => '<i class="entypo-shopping-bag"></i> Cart',
                'search' => '<i class="entypo-magnifying-glass"></i> Search',
            ),
        ),
        array(
            'type' => 'divider',
        ),
        array(
            'name'  => 'header_scripts',
            'label' => __('Header Script', 'claudio'),
            'desc'  => __('Enter your custom scripts here like Google Analytics script', 'claudio'),
            'type'  => 'code_editor',
        ),
    );

    $options['fields']['breadcrumb'] = array(
        array(
            'name'    => 'breadcrumb',
            'label'   => __('Breadcrumb', 'claudio'),
            'desc'    => __('Enable to show a breadcrumb bellow the site header', 'claudio'),
            'type'    => 'switcher',
            'default' => true,
        ),
        array(
            'name'    => 'show_breadcrumb',
            'label'   => __('Show Breadcrumb On', 'claudio'),
            'desc'    => __('Select which page you want to show breadcrumb', 'claudio'),
            'type'    => 'checkbox_list',
            'default' => array('shop', 'product', 'blog', 'post', 'page',),
            'options' => array(
                'shop'    => __('Shop', 'claudio'),
                'product' => __('Singular product', 'claudio'),
                'blog'    => __('Blog', 'claudio'),
                'post'    => __('Singular blog post', 'claudio'),
                'page'    => __('Singular page', 'claudio'),
            ),
        ),
    );

    $options['fields']['content'] = array(
        array(
            'name'    => 'show_author_box',
            'label'   => __('Show author box', 'claudio'),
            'type'    => 'switcher',
            'default' => true,
        ),
        array(
            'name'    => 'show_share_box',
            'label'   => __('Show share box', 'claudio'),
            'type'    => 'switcher',
            'default' => true,
        ),
        array(
            'type' => 'divider'
        ),
        array(
            'name'    => 'excerpt_length',
            'label'   => __('Excerpt Length', 'claudio'),
            'type'    => 'number',
            'size'    => 'small',
            'default' => 30,
        ),
        array(
            'name'    => 'excerpt_more',
            'label'   => __('Show read more link', 'claudio'),
            'desc'    => __('Show read more link after post excerpt', 'claudio'),
            'type'    => 'switcher',
            'default' => true,
        ),
    );

    $options['fields']['shop'] = array(
        array(
            'name'    => 'shop_banner',
            'label'   => __('Shop Banner', 'claudio'),
            'desc'    => __('Enable to show a banner after the site header', 'claudio'),
            'type'    => 'switcher',
            'default' => true,
        ),
        array(
            'name'  => 'banner_image',
            'label' => __('Banner Image', 'claudio'),
            'desc'  => __('Upload a banner image', 'claudio'),
            'type'  => 'image',
        ),
        array(
            'name'    => 'banner_text',
            'label'   => __('Banner Text', 'claudio'),
            'desc'    => __('Enter text to display in the center', 'claudio'),
            'subdesc' => __('Some HTML tags are allowed: <code>img</code>, <code>span</code>, <code>p</code>, with 2 special classes: <code>italic</code>, <code>highlight</code>', 'claudio'),
            'type'    => 'textarea',
        ),
        array(
            'type' => 'divider'
        ),
        array(
            'name'    => 'shop_view',
            'label'   => __('Shop View', 'claudio'),
            'desc'    => __('Select default view for shop page', 'claudio'),
            'type'    => 'select',
            'default' => 'grid',
            'options' => array(
                'grid' => __('Grid', 'claudio'),
                'list' => __('List', 'claudio'),
            ),
        ),
        array(
            'name'    => 'product_newness',
            'label'   => __('Product Newness', 'claudio'),
            'type'    => 'number',
            'size'    => 'small',
            'desc'    => __('Display the "New" badge for how many days?', 'claudio'),
            'default' => 3,
        ),
        array(
            'name'    => 'product_columns',
            'label'   => esc_html__('Product Columns Per Page', 'claudio'),
            'type'    => 'select',
            'default' => '',
            'desc'    => esc_html__('Specify how many product columns you want to show on shop page', 'claudio'),
            'options' => array(
                ''  => esc_html__('Default', 'claudio'),
                '3' => esc_html__('3 Columns', 'claudio'),
                '4' => esc_html__('4 Columns', 'claudio'),
                '2' => esc_html__('2 Columns', 'claudio')
            ),
            'subdesc'    => esc_html__('If it is default, there are 4 product columns on shop page without sidebar and there are 3 product columns on shop page with sidebar', 'claudio'),
        ),
        array(
            'name'    => 'products_per_page',
            'label'   => __('Products Per Page', 'claudio'),
            'type'    => 'number',
            'size'    => 'small',
            'desc'    => __('Specify how many products you want to show on shop page', 'claudio'),
            'default' => 12,
        ),
    );

    $options['fields']['footer'] = array(
        array(
            'name'    => 'footer_widgets',
            'label'   => __('Footer Widgets', 'claudio'),
            'type'    => 'switcher',
            'default' => 1,
        ),
        array(
            'name'    => 'footer_widget_columns',
            'label'   => __('Footer Widgets Columns', 'claudio'),
            'desc'    => __('How many sidebar you want to show on footer', 'claudio'),
            'type'    => 'image_toggle',
            'default' => 4,
            'options' => array(
                1 => CLAUDIO_OPTIONS_URL . 'img/footer/one-column.png',
                2 => CLAUDIO_OPTIONS_URL . 'img/footer/two-columns.png',
                3 => CLAUDIO_OPTIONS_URL . 'img/footer/three-columns.png',
                4 => CLAUDIO_OPTIONS_URL . 'img/footer/four-columns.png',
                5 => CLAUDIO_OPTIONS_URL . 'img/footer/five-columns.png',
            )
        ),
        array(
            'type' => 'divider'
        ),
        array(
            'name'    => 'footer_extra',
            'label'   => __('Footer Extra Content', 'claudio'),
            'desc'    => __('This section will be displayed right before the site footer', 'claudio'),
            'subdesc' => __('HTML and shortcodes are allowed', 'claudio'),
            'type'    => 'textarea',
        ),
        array(
            'type' => 'divider'
        ),
        array(
            'name'    => 'footer_layout',
            'label'   => __('Footer Layout', 'claudio'),
            'desc'    => __('Select layout to display footer copyright and social media icons', 'claudio'),
            'type'    => 'select',
            'default' => 'horizontal',
            'options' => array(
                'horizontal' => __('Horizontal Split', 'claudio'),
                'vertical'   => __('Vertical Centered', 'claudio'),
            ),
        ),
        array(
            'name'    => 'footer_color',
            'label'   => __('Footer Color', 'claudio'),
            'type'    => 'toggle',
            'default' => 'dark',
            'options' => array(
                'light' => __('Light', 'claudio'),
                'dark'  => __('Dark', 'claudio'),
            ),
        ),
        array(
            'name'    => 'footer_copyright',
            'label'   => __('Footer Copyright', 'claudio'),
            'subdesc' => __('HTML and shortcodes are allowed', 'claudio'),
            'type'    => 'textarea',
            'default' => __('Copyright &copy; [year]', 'claudio'),
        ),
        array(
            'name'    => 'socials',
            'label'   => __('Socials', 'claudio'),
            'type'    => 'social',
            'subdesc' => __('Click to social icon to add link', 'claudio'),
        ),
        array(
            'type' => 'divider'
        ),
        array(
            'name'  => 'footer_script',
            'label' => __('Footer Scripts', 'claudio'),
            'type'  => 'code_editor',
        ),
    );

    $options['fields']['export'] = array(
        array(
            'name'    => 'backup',
            'label'   => __('Backup Settings', 'claudio'),
            'subdesc' => __('You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options" button above', 'claudio'),
            'type'    => 'backup',
        ),
    );

    return $options;
}

add_filter('claudio_theme_options', 'claudio_theme_option_fields');

/**
 * Generate custom color scheme css
 *
 * @since 1.0
 */
function claudio_generate_custom_color_scheme() {
    parse_str($_POST['data'], $data);

    if (!isset($data['custom_color_scheme']) || !$data['custom_color_scheme']) {
        return;
    }

    $color_1 = $data['custom_color_1'];
    if (!$color_1) {
        return;
    }

    // Getting credentials
    $url = wp_nonce_url('themes.php?page=theme-options');
    if (false === ($creds = request_filesystem_credentials($url, '', false, false, null))) {
        return; // stop the normal page form from displaying
    }

    // Try to get the wp_filesystem running
    if (!WP_Filesystem($creds)) {
        // Ask the user for them again
        request_filesystem_credentials($url, '', true, false, null);

        return;
    }

    global $wp_filesystem;

    // Prepare LESS to compile
    $less = $wp_filesystem->get_contents(THEME_DIR . '/css/color-schemes/mixin.less');
    $less .= ".custom-color-scheme { .color-scheme($color_1); }";

    // Compile
    require THEME_DIR . '/inc/libs/lessc.inc.php';
    $compiler = new lessc;
    $compiler->setFormatter('compressed');
    $css = $compiler->compile($less);

    // Get file path
    $upload_dir = wp_upload_dir();
    $dir = path_join($upload_dir['basedir'], 'custom-css');
    $file = $dir . '/color-scheme.css';

    // Create directory if it doesn't exists
    wp_mkdir_p($dir);
    $wp_filesystem->put_contents($file, $css, FS_CHMOD_FILE);

    wp_send_json_success();
}

add_action('claudio_ajax_generate_custom_css', 'claudio_generate_custom_color_scheme');

/**
 * Load script for theme options
 *
 * @since 1.0.0
 *
 * @param string $hook
 */
function claudio_enqueue_admin_scripts($hook) {
    if ('appearance_page_theme-options' != $hook) {
        return;
    }

    $min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';

    wp_enqueue_script('claudio-admin', THEME_URL . "/js/backend/options$min.js", array('jquery'), THEME_VERSION, true);
}

add_action('admin_enqueue_scripts', 'claudio_enqueue_admin_scripts');
