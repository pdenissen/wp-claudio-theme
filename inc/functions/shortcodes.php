<?php
/**
 * Define theme shortcodes
 *
 * @package Claudio
 */
class Claudio_Shortcodes {
	/**
	 * Store variables for js
	 *
	 * @var array
	 */
	public $l10n = array();

	/**
	 * Check if WooCommerce plugin is actived or not
	 *
	 * @var bool
	 */
	private $wc_actived = false;

	/**
	 * Construction
	 *
	 * @return Claudio_Shortcodes
	 */
	function __construct() {
		$this->wc_actived = function_exists( 'is_woocommerce' );

		$shortcodes = array(
			'section_title',
			'banner',
			'products_carousel',
			'claudio_products',
			'products_ajax',
			'images_carousel',
			'posts_carousel',
			'promotion_image',
			'instagram',
            'newsletter_form',
			'featured_category',
			'video_banner',
			'icon_box',
		);

		foreach ( $shortcodes as $shortcode ) {
			add_shortcode( $shortcode, array( $this, $shortcode ) );
		}
		add_action( 'wp_footer', array( $this, 'footer' ) );
	}

	/**
	 * Load custom js in footer
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function footer() {
		wp_localize_script( 'claudio', 'claudioShortCode', $this->l10n );
	}
	/**
	 * Shortcode year
	 * Display current year
	 *
	 * @since 1.0
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function year( $atts, $content = null ) {
		return date( 'Y' );
	}

	/**
	 * Shortcode to display a link back to the site.
	 *
	 * @since 1.0
	 * @param array $atts Shortcode attributes
	 * @return string
	 */
	function site_link( $atts ) {
		$name = get_bloginfo( 'name' );
		return '<a class="site-link" href="' . esc_url( get_home_url() ) . '" title="' . esc_attr( $name ) . '" rel="home">' . $name . '</a>';
	}

	/**
	 * Shortcode to display section title
	 *
	 * @param  array $atts
	 * @param  string $content
	 * @return string
	 */
	function section_title( $atts, $content ) {
		$atts = shortcode_atts(array(
			'title'      => '',
			'class_name' => '',
		), $atts );

		if( $content ) {
			$content = sprintf( '<div class="desc">%s</div>', $content );
		} else {
			$atts['class_name'] .= ' no-desc';
		}

		if( $atts['title'] ) {
			$atts['title'] = sprintf( '<h2>%s</h2>', $atts['title'] );
		} else {
			$atts['class_name'] .= ' no-title';
		}

		return sprintf(
			'<div class="section-title %s">%s %s</div>',
			$atts['class_name'],
			$atts['title'],
			$content
		);
	}

	/**
	 * Shortcode to display banner
	 *
	 * @param  array $atts
	 * @param  string $content
	 * @return string
	 */
	function banner( $atts, $content ) {
		$atts = shortcode_atts(array(
			'image'       => '',
			'overlay'     => '',
			'desc'        => '',
			'class_name'  => '',
			'button_text' => '',
			'button_link' => '',
			'footer_text' => '',
			'footer_link' => '',
			'text_align'  => 'align-center',
		), $atts );

		$output = array();

		$title = array();
		if( $content ) {
			if( function_exists( 'wpb_js_remove_wpautop') ) {
				$content = wpb_js_remove_wpautop( $content, true );
			}
			$titles = explode( "\n", $content );
			if( $titles ) {
				foreach ( $titles as $t ) {
					if( $t ) {
						$title[] =  $t;
					}
				}
			}
		}

		$image = '';
		if( $atts['image'] ) {
			$image = wp_get_attachment_image_src( $atts['image'], 'full' );
			if( $image ) {
				$image =	sprintf( '<img alt="%s" src="%s">',
					esc_attr( $atts['image'] ),
					esc_url( $image[0] )
				);
			}
		}

		if( $atts['desc'] ) {
			$output[]  = sprintf( '<p class="b-desc">%s</p>',
				$atts['desc']
			);
		}

		if( $atts['button_text'] ) {
			$output[] = sprintf( '<a href="%s" class="btn-banner">%s</a>',
				esc_url( $atts['button_link'] ),
				$atts['button_text']
			);
		}

		$footer= '';
		if( $atts['footer_text'] ) {
			$footer = sprintf( '<a href="%s" class="footer-link">%s</a>',
				esc_url( $atts['footer_link'] ),
				$atts['footer_text']
			);
		}

		$atts['class_name'] .= ' ' . $atts['text_align'];

		$overlay = '';
		if( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
		}

		return sprintf(
			'<div class="claudio-banner %s">
				<div class="b-overlay" %s></div>
				%s
				<div class="b-content">
					<div class="b-title">%s</div>%s
				</div>
				<div class="b-footer">%s</div>
			</div>',
			$atts['class_name'],
			$overlay,
			$image,
			implode( '', $title),
			implode( '', $output),
			$footer
		);
	}

    /**
     * Shortcode to display newsletter.
     *
     * @since 1.0
     * @param array $atts Shortcode attributes
     * @return string
     */
    function newsletter_form( $atts, $content ) {
        $atts = shortcode_atts( array(
            'style'      => '',
            'class_name' => '',
        ), $atts );

        if( $atts['style'] ) {
            $atts['class_name'] .= ' ' . $atts['style'];
        }

        return sprintf(
            '<div class="claudio-newsletter %s">%s</div>',
            esc_attr( $atts['class_name'] ),
            $this->get_newsletter_form()
        );
    }

	/**
	 * Products Slider shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function products_carousel( $atts, $content ) {
		$atts = shortcode_atts( array(
			'products'        => 'recent',
			'category'        => '',
			'per_page'        => 12,
			'columns'         => 4,
			'orderby'         => '',
			'order'           => '',
			'auto_play'       => false,
			'hide_navigation' => false,
			'class_name'      => '',
		), $atts );

		$output = $this->get_products( $atts );

		$id = uniqid( 'products-carousel-' );
		$this->l10n['productsCarousel'][$id] = array(
			'number'     => $atts['columns'],
			'autoplay'   => $atts['auto_play'],
			'navigation' => ! $atts['hide_navigation'],
		);

		return sprintf(
			'<div class="products-carousel section-products woocommerce %s" id="%s">%s</div>',
			esc_attr( $atts['class_name'] ),
			esc_attr( $id ),
			$output
		);
	}

	/**
	 * Products ajax shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function claudio_products( $atts, $content ) {
		$atts = shortcode_atts( array(
			'products'       => 'recent',
			'category'       => '',
			'per_page'       => 4,
			'columns'        => 4,
			'orderby'        => '',
			'order'          => '',
			'view_more_link' => '',
			'view_more_text' => '',
			'class_name'     => '',
		), $atts );

		$atts['columns'] = intval( $atts['columns'] );
		if( ! $atts['columns'] ) {
			$atts['columns'] = 4;
		}

		$output = $this->get_products( $atts );

		if( $atts['view_more_text'] ) {
			$output .= sprintf( '<div class="products-button"><a href="%s" class="view-more-products">%s</a></div>',
				esc_url( $atts['view_more_link'] ),
				$atts['view_more_text']
			);
		}

		return sprintf(
			'<div class="claudio-products section-products woocommerce %s">%s</div>',
			esc_attr( $atts['class_name'] ),
			$output
		);
	}

	/**
	 * Products ajax shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function products_ajax( $atts, $content ) {
		$atts = shortcode_atts( array(
			'products'           => 'recent',
			'category'           => '',
			'per_page'           => 9,
			'columns'            => 3,
			'orderby'            => 'date',
			'order'              => 'desc',
			'view_more_text'     => __( 'View More Products', 'claudio' ),
			'class_name'         => '',
		), $atts );

		if( isset( $_GET['columns'] ) && $_GET['columns'] ) {
			$atts['columns'] = $_GET['columns'];
		}
		$atts['columns'] = intval( $atts['columns'] );

		if( isset( $_GET['products'] ) && $_GET['products'] ) {
			$atts['products'] = $_GET['products'];
		}

		if( isset( $_GET['odb'] ) && $_GET['odb'] ) {
			$atts['orderby'] = $_GET['odb'];
		}

		if( isset( $_GET['od'] ) && $_GET['od'] ) {
			$atts['order'] = $_GET['od'];
		}

		if( isset( $_GET['id'] ) && $_GET['id'] ) {
			$id = $_GET['id'];
		} else {
			$id = uniqid( 'products-ajax-' );
		}

		$output = $this->get_products( $atts );

		$output .= '<div class="ajax-loading"><i class="fa fa-spin fa-spinner"></i></div>';

		$href = '?products=' . $atts['products'] . '&columns=' . $atts['columns'] . '&odb=' . $atts['orderby'] .'&od=' . $atts['order'] . '&id=' . $id;
		if( $atts['view_more_text'] ) {
			$output .= sprintf( '<div class="products-button"><a href="%s" data-number="%s" class="view-more-products">%s</a></div>',
				esc_attr( $href ),
				esc_attr( $atts['per_page'] ),
				$atts['view_more_text']
			);
		}

		return sprintf(
			'<div class="claudio-products-ajax section-products woocommerce %s" id="%s">%s</div>',
			esc_attr( $atts['class_name'] ),
			esc_attr( $id ),
			$output
		);
	}

	/**
	 * Helper function, get products for shortcodes
	 *
	 * @param array  $atts
	 *
	 * @return string
	 */
	function get_products( $atts ) {
        global $woocommerce_loop;

        if ( ! $this->wc_actived ) {
        	return '';
        }

		$atts = shortcode_atts( array(
			'products'           => 'recent',
			'category'           => '',
			'per_page'           => '',
			'columns'            => '',
			'orderby'            => '',
			'order'              => '',
		), $atts );

		$output = '';
		$meta_query = WC()->query->get_meta_query();

		$args = array(
			'post_type'           => 'product',
			'post_status'         => 'publish',
			'ignore_sticky_posts' => 1,
			'posts_per_page'      => $atts['per_page']
		);

		if ( $atts['category'] ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'product_cat',
					'terms'    => $atts['category'],
					'field'    => 'slug',
				)
			);
		}

		if ( $atts['products'] == 'recent' ) {
			$args['orderby'] = 'date';
			$args['order']   = 'desc';
		} elseif ( $atts['products'] == 'featured' ) {
			$args['orderby'] = $atts['orderby'];
			$args['order']   = $atts['order'];

			$meta_query[] = array(
				'key'   => '_featured',
				'value' => 'yes',
			);
		} elseif ( $atts['products'] == 'best_selling' ) {
			$args['orderby']  = 'meta_value_num';
			$args['order']    = 'desc';
			$args['meta_key'] = 'total_sales';
		} elseif ( $atts['products'] == 'top_rated' ) {
			$args['orderby'] = $atts['orderby'];
			$args['order']   = $atts['order'];

			add_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );
		} elseif ( $atts['products'] == 'sale' ) {
			$args['orderby']       = $atts['orderby'];
			$args['order']         = $atts['order'];

			$product_ids_on_sale   = wc_get_product_ids_on_sale();
			$args['post__in']      = array_merge( array( 0 ), $product_ids_on_sale );
			$args['no_found_rows'] = 1;
		}

		$args['meta_query'] = $meta_query;
		ob_start();

		$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
		$args['paged'] = $paged;

		$products = new WP_Query( $args );
		$woocommerce_loop['columns'] = $atts['columns'];
		if ( $products->have_posts() ) :

			woocommerce_product_loop_start();

			while ( $products->have_posts() ) : $products->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile; // end of the loop.

			woocommerce_product_loop_end();

		endif;

		wp_reset_postdata();

		remove_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );

		$output .= ob_get_clean();

		return $output;
	}

	/**
	 * Images carousel shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function images_carousel( $atts, $content ) {
		$atts = shortcode_atts( array(
			'images'              => '',
			'image_size'          => 'thumbnail',
			'custom_links'        => '',
			'custom_links_target' => '_self',
			'number'              => '4',
			'auto_play'           => false,
			'hide_navigation'     => false,
			'class_name'          => '',
		), $atts );

		$output = array();
		$number = intval( $atts['number'] );
		$number = $number ? $number : '4';
		$custom_links = $atts['custom_links'] ? explode( "\n", $atts['custom_links'] ) : '';

		$images = $atts['images'] ? explode( ',', $atts['images'] ) : '';

		$id = uniqid( 'images-carousel-' );
		$this->l10n['imagesCarousel'][$id] = array(
			'number'     => $number,
			'autoplay'   => $atts['auto_play'],
			'navigation' => ! $atts['hide_navigation'],
		);
		if( $images ) {
			$i= 0;
			foreach ( $images as $attachment_id ) {
				$image = wp_get_attachment_image_src( $attachment_id, $atts['image_size'] );
				if( $image ) {
					$link = '';
					if( $custom_links && isset( $custom_links[$i] ) ) {
						$link = str_replace( 'br', '', $custom_links[$i] );
						$link = 'href="' . esc_url( $link ) . '"';
					}
					$output[] =	sprintf( '<div class="item"><a %s target="%s"><img alt="%s" src="%s"></a></div>',
						$link,
						esc_attr( $atts['custom_links_target'] ),
						esc_attr( $attachment_id ),
						esc_url( $image[0] )
					);
				}
				$i++;
			}
		}

		return sprintf( '<div id="%s" class="bb-images-carousel %s"><div class="bb-owl-list">%s</div></div>',
			esc_attr( $id ),
			esc_attr( $atts['class_name'] ),
			implode( '', $output )
		);
	}

	/**
	 * Posts carousel shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function posts_carousel( $atts, $content ) {
		$atts = shortcode_atts( array(
			'total'           => 6,
			'categories'      => '',
			'order_by'        => 'date',
			'order'           => 'desc',
			'excerpt_length'  => 15,
			'read_more_text'  => __( 'Read More', 'claudio' ),
			'number'          => 2,
			'auto_play'       => false,
			'hide_navigation' => false,
			'class_name'      => '',
		), $atts );

		$output = array();
		$number = intval( $atts['number'] );
		$number = $number ? $number : '2';

		$id = uniqid( 'posts-carousel-' );
		$this->l10n['postsCarousel'][$id] = array(
			'number'     => $number,
			'autoplay'   => $atts['auto_play'],
			'navigation' => ! $atts['hide_navigation'],
		);

		$query_args = array(
			'orderby'             => $atts['order_by'],
			'order'               => $atts['order'],
			'posts_per_page'      => $atts['total'],
			'post_type'           => 'post',
			'ignore_sticky_posts' => true,
		);
		if ( ! empty( $atts['categories'] ) ) {
			$query_args['category_name'] = $atts['categories'];
		}
		$query = new WP_Query( $query_args );

		while ( $query->have_posts() ) : $query->the_post();

			$src = claudio_get_image( array(
				'size'   => 'claudio-blog-small-thumb',
				'format' => 'src',
				'echo'   => false,
			) );

			$class_thumb = 'no-thumb';
			$article     = array();

			if ( $src ) {
				$class_thumb = '';

				$article[] = sprintf(
					'<a class="post-thumb" href="%s" title="%s"><img src="%s" alt="%s"></a>',
					esc_url( get_permalink() ),
					the_title_attribute( 'echo=0' ),
					esc_url( $src ),
					the_title_attribute( 'echo=0' )
				);
			}

			$article[] = sprintf( '<div class="post-text"><a class="post-title" href="%s" title="%s" rel="bookmark">%s</a>',
				esc_url( get_permalink() ),
				the_title_attribute( 'echo=0' ),
				get_the_title()
			);

			$comments_count =  wp_count_comments( get_the_ID() );
			$article[] = sprintf( '
				<div class="post-meta">
					<span class="post-author">
						<i class="fa fa-edit"></i>
						<a class="url fn n" href="%s" title="%s" rel="author">%s</a>
					</span>
					<span class="post-comments">
						<i class="fa fa-comment-o"></i>
						%s
					</span>
				</div>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( get_the_author() ),
				get_the_author(),
				intval( $comments_count->total_comments ) . __( ' Comments', 'claudio' )
			);

			$article[] = claudio_content_limit( get_the_excerpt(), intval( $atts['excerpt_length'] ), '', false );
			if ( $atts['read_more_text'] ) {
				$article[] = sprintf( '<a class="read-more" href="%s" title="%s" rel="bookmark">%s</a>',
					esc_url( get_permalink() ),
					the_title_attribute( 'echo=0' ),
					$atts['read_more_text']
				);
			}

			$output[] = sprintf( '<div class="bb-post %s">%s</div></div>',
				esc_attr( $class_thumb ),
				implode( '', $article )
			);
		endwhile;
		wp_reset_postdata();

		return sprintf( '<div id="%s" class="bb-posts-carousel %s"><div class="bb-owl-list">%s</div></div>',
			esc_attr( $id ),
			esc_attr( $atts['class_name'] ),
			implode( '', $output )
		);
	}

	/**
	 * Promotion image shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function promotion_image( $atts, $content ) {
		$atts = shortcode_atts( array(
			'subtitle'           => '',
			'image'              => '',
			'overlay'            => '',
			'link'               => '',
			'custom_link_target' => '_self',
			'class_name'         => '',
		), $atts );

		$output = '';

		$overlay = '';
		if( $atts['overlay'] ) {
			$overlay = 'style="background-color:' . esc_attr( $atts['overlay'] ) . '"';
		}

		if( $atts['image'] ) {
			$image = wp_get_attachment_image_src( $atts['image'], 'full' );
			if( $image ) {
				$output = sprintf(
					'<a href="%s" target="%s" class="promotion-image">
						<span class="overlay" %s></span>
						<img alt="%s" src="%s">
						<span class="title">
							<span>%s</span>
							<strong>%s</strong>
						</span>
					</a>',
					esc_url( $atts['link'] ),
					esc_attr( $atts['custom_link_target'] ),
					$overlay,
					esc_attr( $content ),
					esc_url( $image[0] ),
					$atts['subtitle'],
					$content
				);
			}
		}

		return $output;
	}

	/**
	 * Featured_category shortcode
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	function featured_category( $atts, $content ) {
		$atts = shortcode_atts( array(
			'category'   => '',
			'image'      => '',
			'image_size' => 'full',
			'class_name' => '',
		), $atts );

		if ( ! $this->wc_actived ) {
			return '';
		}

		if( ! $atts['category'] ) {
			return '';
		}
		$category = get_term_by( 'slug', $atts['category'], 'product_cat' );

		$image = '';
		if( $atts['image'] ) {
			$images = wp_get_attachment_image_src( $atts['image'], $atts['image_size'] );
			if( $images ) {
				$image = sprintf( '<img alt="%s" src="%s">',
					esc_attr( $category->name ),
					$images[0]
				);
			}
		}

		return sprintf( '<div class="featured-cat %s"><a class="cat-title" href="%s" title="%s">%s <span>%s</span></a></div>',
			esc_attr( $atts['class_name'] ),
			esc_url( get_term_link( $atts['category'], 'product_cat' ) ),
			esc_attr( $category->name ),
			$image,
			$category->name
		);
	}

	/**
	 * Get instagram shortcode
	 *
	 * @since  1.0
	 *
	 * @return string
	 */
	function instagram( $atts, $content ) {
		$atts = shortcode_atts( array(
			'user_id'      => '',
			'access_token' => '',
			'numbers'      => 8,
			'class_name'   => '',
		), $atts, 'instagram' );

		if ( empty( $atts['user_id'] ) ) {
			return '';
		}

		$output = '';
		$instagram = $this->scrape_instagram( $atts['user_id'] );

		if ( is_wp_error( $instagram ) ) {
			return $instagram->get_error_message();
		}

		$count = 1;
		foreach ( $instagram as $data ) {
			if ( $count > $atts['numbers'] ) {
				break;
			}

			$output .= '<li><a target="_blank" href="'. esc_url( $data['link'] ) . '"><img src="' . esc_url( $data['large'] ) . '" alt="' . esc_attr( $data['description'] ) . '"></a></li>';

			$count++;
		}

		if( $content ) {
			$content = '<div class="section-title col-sm-offset-2 col-md-offset-2 col-md-8 col-sm-8">' . $content . '</div>';
		}

		return sprintf( '<div class="claudio-instagram %s"> %s <ul class="ins-list clearfix">%s</ul></div>',
			esc_attr( $atts['class_name'] ),
			$content,
			$output
		);
	}

	/**
	 * Video banner shortcode
	 *
	 * @since  1.0
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function video_banner( $atts, $content = null ) {
		$atts = shortcode_atts( array(
			'video'      => '',
			'image'      => '',
			'mute'       => '',
			'height'     => 'auto',
			'class_name' => '',
		), $atts );

		if ( empty( $atts['video'] ) ) {
			return '';
		}

		$ext   = wp_check_filetype( $atts['video'] );
		$image = wp_get_attachment_image_src( intval( $atts['image'] ), 'full' );

		return sprintf(
			'<div class="video-banner %s">
				<video height="%d" preload="none" poster="%s" loop %s>
					<source src="%s" type="%s">
				</video>
				<i class="fa fa-play"></i>
			</div>',
			esc_attr( $atts['class_name'] ),
			esc_attr( $atts['height'] ),
			esc_url( $image[0] ),
			! empty( $atts['mute'] ) ? 'muted' : '',
			esc_url( $atts['video'] ),
			esc_attr( $ext['type'] )
		);
	}

	/**
	 * Icon box shortcode
	 *
	 * @param  array $atts
	 * @param  string $content
	 *
	 * @return string
	 */
	function icon_box( $atts, $content ) {
		$atts = shortcode_atts( array(
			'title'          => '',
			'subtitle'       => '',
			'icon'           => '',
			'icon_position'  => 'left',
			'read_more_text' => '',
			'link'           => '',
			'class_name'     => '',
		), $atts );

		if ( $content ) {
			$content = '<div class="box-content">' . do_shortcode( $content ) . '</div>';
		}

		if ( $atts['read_more_text'] && $atts['link'] ) {
			$content .= '<a class="icon-read-more" href="' . esc_url( $atts['link'] ) . '">' . esc_html( $atts['read_more_text'] ) . '</a>';
		}

		return sprintf(
			'<div class="icon-box icon-%s %s">
				<i class="%s"></i>
				%s
				%s
				%s
			</div>',
			esc_attr( $atts['icon_position'] ),
			esc_attr( $atts['class_name'] ),
			esc_attr( $atts['icon'] ),
			! empty( $atts['title'] ) ? "<h3>{$atts['title']}</h3>" : '',
			! empty( $atts['subtitle'] ) ? "<h5>{$atts['subtitle']}</h5>" : '',
			$content
		);
	}


    /**
     * Get newsletter form
     *
     * @return string
     */
    function get_newsletter_form() {
        if( ! class_exists( 'NewsletterSubscription' ) ) {
            return '';
        }
        $options_profile = get_option('newsletter_profile');
        $form = NewsletterSubscription::instance()->get_form_javascript();

        $form .= '<form action="' . plugins_url('newsletter/do/subscribe.php') . '" onsubmit="return newsletter_check(this)" method="post">';
        // Referrer
        $form .= '<input type="hidden" name="nr" value="widget"/>';

        if ($options_profile['name_status'] == 2)
            $form .= '<p><input class="newsletter-firstname" type="text" name="nn" value="' . esc_attr($options_profile['name']) . '" onclick="if (this.defaultValue==this.value) this.value=\'\'" onblur="if (this.value==\'\') this.value=this.defaultValue"/></p>';

        if ($options_profile['surname_status'] == 2)
            $form .= '<p><input class="newsletter-lastname" type="text" name="ns" value="' . esc_attr($options_profile['surname']) . '" onclick="if (this.defaultValue==this.value) this.value=\'\'" onblur="if (this.value==\'\') this.value=this.defaultValue"/></p>';

        $form .= '<p><input class="newsletter-email" type="email" required name="ne" value="' . esc_attr($options_profile['email']) . '" onclick="if (this.defaultValue==this.value) this.value=\'\'" onblur="if (this.value==\'\') this.value=this.defaultValue"/></p>';

        if (isset($options_profile['sex_status']) && $options_profile['sex_status'] == 2) {
            $form .= '<p><select name="nx" class="newsletter-sex">';
            $form .= '<option value="m">' . $options_profile['sex_male'] . '</option>';
            $form .= '<option value="f">' . $options_profile['sex_female'] . '</option>';
            $form .= '</select></p>';
        }

        // Extra profile fields
        for ($i = 1; $i <= NEWSLETTER_PROFILE_MAX; $i++) {
            if ($options_profile['profile_' . $i . '_status'] != 2)
                continue;
            if ($options_profile['profile_' . $i . '_type'] == 'text') {
                $form .= '<p><input class="newsletter-profile newsletter-profile-' . $i . '" type="text" name="np' . $i . '" value="' . $options_profile['profile_' . $i] . '" onclick="if (this.defaultValue==this.value) this.value=\'\'" onblur="if (this.value==\'\') this.value=this.defaultValue"/></p>';
            }
            if ($options_profile['profile_' . $i . '_type'] == 'select') {
                $form .= '<p>' . $options_profile['profile_' . $i] . '<br /><select class="newsletter-profile newsletter-profile-' . $i . '" name="np' . $i . '">';
                $opts = explode(',', $options_profile['profile_' . $i . '_options']);
                for ($t = 0; $t < count($opts); $t++) {
                    $form .= '<option>' . trim($opts[$t]) . '</option>';
                }
                $form .= '</select></p>';
            }
        }

        $lists = '';
        for ($i = 1; $i <= NEWSLETTER_LIST_MAX; $i++) {
            if ($options_profile['list_' . $i . '_status'] != 2)
                continue;
            $lists .= '<input type="checkbox" name="nl[]" value="' . $i . '"';
            if ($options_profile['list_' . $i . '_checked'] == 1)
                $lists .= ' checked';
            $lists .= '/>&nbsp;' . $options_profile['list_' . $i] . '<br />';
        }
        if (!empty($lists))
            $form .= '<p>' . $lists . '</p>';


        $extra = apply_filters('newsletter_subscription_extra', array());
        foreach ($extra as &$x) {
            $form .= "<p>";
            if (!empty($x['label']))
                $form .= $x['label'] . "<br/>";
            $form .= $x['field'] . "</p>";
        }

        if ($options_profile['privacy_status'] == 1) {
            if (!empty($options_profile['privacy_url'])) {
                $form .= '<p><input type="checkbox" name="ny"/>&nbsp;<a target="_blank" href="' . $options_profile['privacy_url'] . '">' . $options_profile['privacy'] . '</a></p>';
            }
            else
                $form .= '<p><input type="checkbox" name="ny"/>&nbsp;' . $options_profile['privacy'] . '</p>';
        }

        if (strpos($options_profile['subscribe'], 'http://') !== false) {
            $form .= '<p><input class="newsletter-submit" type="image" src="' . $options_profile['subscribe'] . '"/></p>';
        } else {
            $form .= '<p><input class="newsletter-submit" type="submit" value="' . $options_profile['subscribe'] . '"/></p>';
        }

        $form .= '</form>';

        return $form;
    }

    /**
	 * Scrape Instagram for images.
	 *
	 * @see https://wordpress.org/plugins/wp-instagram-widget/
	 * @param string $username
	 * @param int    $cache_time Cache time
	 * @return mixed|WP_Error
	 */
	protected function scrape_instagram( $username, $cache_time = 3600 )
	{
		$username = strtolower( $username );
		$username = str_replace( '@', '', $username );

		if ( false === ( $instagram = get_transient( 'instagram-a5-' . sanitize_title_with_dashes( $username ) ) ) )
		{
			$remote = wp_remote_get( 'http://instagram.com/' . trim( $username ) );

			if ( is_wp_error( $remote ) )
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'claudio' ) );

			if ( 200 != wp_remote_retrieve_response_code( $remote ) )
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'claudio' ) );

			$shards      = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json  = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], true );

			if ( ! $insta_array )
				return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'claudio' ) );

			if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) )
			{
				$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
			}
			else
			{
				return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'claudio' ) );
			}

			if ( ! is_array( $images ) )
				return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'claudio' ) );

			$instagram = array();

			foreach ( $images as $image )
			{
				$image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
				$image['display_src']   = preg_replace( '/^https?\:/i', '', $image['display_src'] );

				// handle both types of CDN url
				if ( ( strpos( $image['thumbnail_src'], 's640x640' ) !== false ) )
				{
					$image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
					$image['small']     = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
				}
				else
				{
					$urlparts  = wp_parse_url( $image['thumbnail_src'] );
					$pathparts = explode( '/', $urlparts['path'] );
					array_splice( $pathparts, 3, 0, array( 's160x160' ) );
					$image['thumbnail'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
					$pathparts[3]       = 's320x320';
					$image['small']     = '//' . $urlparts['host'] . implode( '/', $pathparts );
				}

				$image['large'] = $image['thumbnail_src'];

				if ( $image['is_video'] == true )
				{
					$type = 'video';
				}
				else
				{
					$type = 'image';
				}

				$caption = __( 'Instagram Image', 'claudio' );
				if ( ! empty( $image['caption'] ) )
				{
					$caption = $image['caption'];
				}

				$instagram[] = array(
					'description' => $caption,
					'link'        => trailingslashit( '//instagram.com/p/' . $image['code'] ),
					'time'        => $image['date'],
					'comments'    => $image['comments']['count'],
					'likes'       => $image['likes']['count'],
					'thumbnail'   => $image['thumbnail'],
					'small'       => $image['small'],
					'large'       => $image['large'],
					'original'    => $image['display_src'],
					'type'        => $type,
				);
			}

			// do not set an empty transient - should help catch private or empty accounts
			if ( ! empty( $instagram ) )
			{
				$instagram = base64_encode( serialize( $instagram ) );
				set_transient( 'instagram-a5-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'null_instagram_cache_time', $cache_time ) );
			}
		}

		if ( ! empty( $instagram ) )
		{
			return unserialize( base64_decode( $instagram ) );
		}

		return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'claudio' ) );
	}
}
