<?php
/**
 * Load and register widgets
 *
 * @package Claudio
 */

require_once THEME_DIR . '/inc/widgets/recent-posts.php';
require_once THEME_DIR . '/inc/widgets/tabs.php';
require_once THEME_DIR . '/inc/widgets/mini-cart.php';

/**
 * Register widgets
 *
 * @since  1.0
 *
 * @return void
 */
function claudio_register_widgets() {
	register_widget( 'Claudio_Recent_Posts_Widget' );
	register_widget( 'Claudio_Tabs_Widget' );
	register_widget( 'Claudio_Mini_Cart_Widget' );
}
add_action( 'widgets_init', 'claudio_register_widgets' );
